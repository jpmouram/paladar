<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Carrinho extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
		$this->load->model('categorias_model');
		$this->load->model('clientes_model');
		$this->load->model('produtos_model');
		$this->load->model('subcategorias_model');
		$this->load->helper(array('form', 'url'));
    }

	public function index($nomeCategoria = null, $nomeSubCategoria = null, $nomeProduto = null, $idProduto = null)
	{
	}
	
	public function add()
	{
		$produto_id = $this->input->post('produto_id');
		$produto_nome = $this->input->post('produto_nome');
		$produto_valor = $this->input->post('produto_valor');
		$qtd = 1;
		if($this->input->post('produto_qtd') > 0){
			$qtd = $this->input->post('produto_qtd');
		}
		if(@$_SESSION['carrinho'][$produto_id]){
			$qtd = ($_SESSION['carrinho'][$produto_id]['qtd']+1);
		}
		$_SESSION['carrinho'][$produto_id] = array('nome' => $produto_nome, 'qtd' => $qtd, 'valor_unit' => $produto_valor, 'valor_total' => ($produto_valor * $qtd));
		$html = '';
		foreach($_SESSION['carrinho'] as $k => $v){
			$html .= '
			<tr>
				<td>'.$v['nome'].'</td>
				<td align="center">'.$v['qtd'].'</td>
				<td align="center">'.number_format($v['valor_unit'],2,",",".").'</td>
				<td align="center">'.number_format($v['valor_total'],2,",",".").'</td>
			  </tr>';
		}
		echo '<table class="table table-striped">
		<thead>
		  <tr>
			<th>Produto</th>
			<th style="text-align:center;">Qtd</th>
			<th style="text-align:center;">Unit</th>
			<th style="text-align:center;">Total</th>
		  </tr>
		</thead>
		<tbody>
		  '.$html.'
		</tbody>
	  </table>';
	}
	
	public function checkout()
	{
		if(!$this->session->has_userdata('carrinho')){redirect(base_url());}
		$categorias = $this->categorias_model->listar();
		$data['categorias'] = $categorias;
		$this->load->view('includes/headers', $data);
		$this->load->view('checkout', $data);
		$this->load->view('includes/footer');
	}
	
	public function remover($produto_id = null){
		unset($_SESSION['carrinho'][$produto_id]);
		redirect(base_url('carrinho/checkout'));
	}
	
	public function finalizar(){
		if(!$this->session->has_userdata('carrinho')){redirect(base_url());}
		$array_pedido = array('status_pedido' => '1', 'id_cliente' => $_SESSION['session_cliente']->id, 'data_add' => date('Y-m-d H:i:s'));
		$this->db->insert('pedidos', $array_pedido);
		$id_pedido = $this->db->insert_id();
		foreach($_SESSION['carrinho'] as $k => $v){
			$array_pedidos_det = array('id_pedido' => $id_pedido, 'id_produto' => $k, 'qtd' => $v['qtd'], 'valor_unit' => $v['valor_unit'], 'valor_total' => ($v['valor_unit'] * $v['qtd']));
			$this->db->insert('pedidos_det', $array_pedidos_det);
		}
		$this->session->unset_userdata('carrinho');
		redirect('clientes/painel');
	}
	
	public function listar_produtos()
	{
		$data['html'] = '';
		$c=0;
		foreach($this->input->post('produtos') as $k => $produto){
			$c++;
			$id = $produto['add'];
			$dadosProduto = $this->produtos_model->get_by_id($id);
			$dadosProduto = $dadosProduto[0];
			$valor = ($produto['amount'] - $produto['discount_amount']);
			$data['html'] .= '
			<tr class="rem'.$c.'">
				<td class="invert">'.$c.'</td>
				<td class="invert-image"><a href="'.base_url('produtos/visualizar/'.$id.'/'.$produto['item_name']).'"><img src="'.base_url('imagens/produtos/'.$dadosProduto->imagem).'" alt=" " width="70"></a></td>
				<td class="invert">
					 <div class="quantity"> 
						<div class="quantity-select">                           
							<div class="entry value-minus" id="minus_'.$c.'">&nbsp;</div>
							<div class="entry value"><span>1</span></div>
							<div class="entry value-plus active" id="plus_'.$c.'">&nbsp;</div>
						</div>
					</div>
				</td>
				<td class="invert">'.$produto['item_name'].'</td>
				
				<td class="invert">'.number_format($valor, 2,",",".").'</td>
				<td class="invert">
					<div class="rem">
						<div class="close1"> </div>
					</div>

				</td>
			</tr>';
		}
		echo $data['html'];
	}
	
}

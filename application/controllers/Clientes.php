<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientes extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
		$this->load->model('categorias_model');
		$this->load->model('clientes_model');
		$this->load->model('produtos_model');
		$this->load->model('subcategorias_model');
		$this->load->helper(array('form', 'url'));
    }

	public function index($nomeCategoria = null, $nomeSubCategoria = null, $nomeProduto = null, $idProduto = null)
	{
	}
	
	public function cadastro()
	{
		$categorias = $this->categorias_model->listar();
		$data['categorias'] = $categorias;
		$this->load->view('includes/headers', $data);
		$this->load->view('login', $data);
		$this->load->view('includes/footer');
	}
	
	public function salvar($id = null)
	{
		foreach($this->input->post() as $k => $v){
			$data[$k] = $v;
		}
		
		// Add
		if($id == null){
			$data['data_add'] = date('Y-m-d H:i:s');
			
			$verifica_email = $this->clientes_model->get(array('email' => $data['email']));
			if(count($verifica_email) > 0){
				echo '<br><div class="alert alert-danger">
				  <strong>Erro!</strong> Esse e-mail já exíste.
				</div>';
				exit;
			}
			$id = $this->clientes_model->salvar($data);
			
			if($id > 0){
				echo '<br>
				<div class="alert alert-success">
				  <strong>Cadastrado!</strong> Dados salvos com sucesso.
				</div>';
			}
			
		}elseif($id > 0){
			$verifica_email = $this->clientes_model->get(array('email' => $data['email'], 'id !=' => $id));
			if(count($verifica_email) > 0){
				echo '<br><div class="alert alert-danger">
				  <strong>Erro!</strong> Esse e-mail já exíste.
				</div>';
				exit;
			}
			$id_imovel = $this->clientes_model->salvar($data, $id);
			$get_cliente = $this->clientes_model->get_by_id($id);
			$this->session->set_userdata(array('session_cliente' => $get_cliente[0]));
			echo '<br>
			<div class="alert alert-success">
			  <strong>Salvo!</strong> Dados salvos com sucesso.
			</div>';
		}
	}
	
	public function sair()
	{
		$this->session->unset_userdata('session_cliente');
		redirect(base_url('clientes/cadastro'));
	}
	
	public function painel()
	{
		$this->load->model('seguranca_clientes_model');
		$categorias = $this->categorias_model->listar();
		$data['categorias'] = $categorias;
		$this->load->view('includes/headers', $data);
		
		$this->db->select('pedidos.id, pedidos.data_add, pedidos_status.nome as status_pedido, pedidos.id_cliente, SUM(pedidos_det.valor_total) as total_pedido');
		$this->db->from('pedidos');
		$this->db->join('pedidos_det', 'pedidos_det.id_pedido = pedidos.id');
		$this->db->join('pedidos_status', 'pedidos_status.id = pedidos.status_pedido');
		$this->db->where(array('pedidos.status' => 1, 'pedidos.id_cliente' => $_SESSION['session_cliente']->id));
		$query_pedidos = $this->db->group_by('pedidos.id');
		$query_pedidos = $this->db->get();
		$data['pedidos'] = $query_pedidos->result();
		
		$this->load->view('clientes_painel', $data);
		$this->load->view('includes/footer');
	}
	
	public function login()
	{
		foreach($this->input->post() as $k => $v){
			$data[$k] = $v;
		}
		$data['status'] = 1;
		$login = $this->clientes_model->get($data);
		if(count($login) == 1){
			$this->session->set_userdata(array('session_cliente' => $login[0]));
			echo '<br><div class="alert alert-success">Login efetuado com sucesso, aguarde você será redirecionado...</div>';
		}else{
			echo '<br><div class="alert alert-warning">Nenhum cliente foi encontrado.</div>';
		}
	}
	
}

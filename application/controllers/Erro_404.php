<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Erro_404 extends CI_Controller {

	public function index()
	{
		$data = array('titleWebsite' => '404 - Página não exíste');
		$this->load->view('admin/includes/headers', $data);
		$this->load->view('erro_404', $data);
	}
}

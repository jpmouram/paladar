<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
		$this->load->model('categorias_model');
		$this->load->model('subcategorias_model');
		$this->load->helper(array('form', 'url'));
    }

	public function index()
	{
		$categorias = $this->categorias_model->listar();
		$data['categorias'] = $categorias;
		$this->load->view('includes/headers', $data);
		$this->load->view('index_site', $data);
		$this->load->view('includes/footer');
	}
	
	public function produtos($nomeCategoria = null, $nomeSubCategoria = null, $nomeProduto = null, $idProduto)
	{
		$categorias = $this->categorias_model->listar();
		$data['categorias'] = $categorias;
		$this->load->view('includes/headers', $data);
		# $this->load->view('produtos', $data);
		$this->load->view('includes/footer');
	}
	
}

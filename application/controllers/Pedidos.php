<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pedidos extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
		$this->load->helper(array('form', 'url'));
    }

	public function index($nomeCategoria = null, $nomeSubCategoria = null, $nomeProduto = null, $idProduto = null)
	{
	}
	
	public function cancelar($pedido_id = null){
		$this->db->set('status_pedido', 3);
		$this->db->where('id', $pedido_id);
		$this->db->update('pedidos');
		redirect(base_url('clientes/painel'));
	}
	
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produtos extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
		$this->load->model('categorias_model');
		$this->load->model('produtos_model');
		$this->load->model('subcategorias_model');
		$this->load->helper(array('form', 'url'));
    }

	public function index($nomeCategoria = null, $nomeSubCategoria = null, $nomeProduto = null, $idProduto = null)
	{
		$categorias = $this->categorias_model->listar();
		$data['categorias'] = $categorias;
		$this->load->view('includes/headers', $data);
		$this->load->view('produtos', $data);
		$this->load->view('includes/footer');
	}
	
	public function visualizar($idProduto = null, $nomeProduto = null)
	{
		$categorias = $this->categorias_model->listar();
		$data['categorias'] = $categorias;
		$where = array();
		if($idProduto <> ''){
			$where['a.id'] = urldecode($idProduto);
		}
		
		$produtos = $this->produtos_model->listar($where);
		$this->load->view('includes/headers', $data);
		$data['produtos'] = $produtos;
		$this->load->view('produto', $data);
		$this->load->view('includes/footer');
	}
	
	public function listar($nomeCategoria = null, $nomeSubCategoria = null, $nomeProduto = null, $idProduto = null)
	{
		$categorias = $this->categorias_model->listar();
		$data['categorias'] = $categorias;
		$where = array();
		if($nomeCategoria <> ''){
			$where['c.categoria'] = urldecode($nomeCategoria);
		}
		
		if($nomeSubCategoria <> ''){
			$where['b.categoria'] = urldecode($nomeSubCategoria);
		}
		
		$produtos = $this->produtos_model->listar($where);
		$this->load->view('includes/headers', $data);
		$data['produtos'] = $produtos;
		$this->load->view('produtos', $data);
		$this->load->view('includes/footer');
	}
	
	public function imovel($id, $titulo){
		$categorias = $this->categorias_model->listar();
		$data['categorias'] = $categorias;
		$this->load->view('includes/headers', $data);
		$this->load->view('index_site', $data);
		$this->load->view('includes/footer');
	}
	
}

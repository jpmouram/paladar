<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categorias extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
		$this->load->model('categorias_model');
    }

	public function index()
	{
	}	
	
	public function salvar($id = null)
	{
		foreach($this->input->post() as $k => $v){
			if(substr_count($k, 'price') > 0){
				$data[$k] = str_replace('.', '', $v);
				$data[$k] = str_replace(',', '.', $data[$k]);
			}else{
				$data[$k] = $v;
			}
		}
		
		// Add
		if($id == null){
			$data['data_add'] = date('Y-m-d H:i:s');
			$id = $this->categorias_model->salvar($data);
			
			if($id > 0){
				echo '
				<div class="alert alert-success">
				  <strong>Cadastrado!</strong> Categoria cadastrada com sucesso.
				</div>';
			}
			
		}elseif($id > 0){
			$id_imovel = $this->categorias_model->salvar($data, $id);
			echo '
			<div class="alert alert-success">
			  <strong>Salvo!</strong> Categoria salva com sucesso.
			</div>';
		}
	}
	
	public function listar($id = null)
	{
		$data = array();
		$this->load->view('admin/includes/headers', array('titleWebsite' => 'Painel Administrativo', 'descSite' => ''));
		$this->load->view('admin/includes/top', array('titleWebsite' => 'Painel Administrativo', 'descSite' => ''));
		
		$where = array();
		$where['a.id_categoria'] = 0;
		if($id > 0){
			$where['b.id_categoria'] = $id;
		}
		
		$lista = $this->categorias_model->listar($where);
		$this->load->view('admin/categorias-listar', array('lista' => $lista));
		$this->load->view('admin/includes/footer', $data);
	}
	
	public function adicionar($id = null)
	{
		$this->load->view('admin/includes/headers', array('titleWebsite' => 'Painel Administrativo', 'descSite' => ''));
		$this->load->view('admin/includes/top', array('titleWebsite' => 'Painel Administrativo', 'descSite' => ''));
		
		$query = $this->db->query("show columns from categorias");
		foreach($query->result() as $row){
			if($row->Field <> 'price'){
				$data[$row->Field] = '';
			}else{
				$data[$row->Field] = 0;
			}
		}
		
		$this->load->view('admin/categorias-add', $data);
		$this->load->view('admin/includes/footer', $data);
	}
	
	public function editar($id = null)
	{
		$this->load->view('admin/includes/headers', array('titleWebsite' => 'Painel Administrativo', 'descSite' => ''));
		$this->load->view('admin/includes/top', array('titleWebsite' => 'Painel Administrativo', 'descSite' => ''));
		
		$dados = $this->categorias_model->get_by_id($id);
		$data = $dados[0];
		
		$this->load->view('admin/categorias-add', $data);
		$this->load->view('admin/includes/footer', $data);
	}
	
	public function excluir($id = null){
		if($id > 0){
			if($this->categorias_model->excluir($id)){
				redirect(base_url('admin/categorias/listar/'));
			}
		}
	}
		
}

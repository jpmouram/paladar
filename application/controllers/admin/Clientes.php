<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientes extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
		$this->load->model('clientes_model');
		# $this->load->model('contatos_model');
    }

	public function index()
	{
		$this->load->view('admin/includes/top', array('titleWebsite' => 'Painel Administrativo - Imobiliária', 'descSite' => 'Aluguel, Temporada, Venda & Arrendamentos'));
		$this->load->view('admin/includes/menu');
		$data = array();
		$this->load->view('admin/clientes-add');
	}
	
	public function excluir($id = null){
		if($id > 0){
			if($this->clientes_model->excluir($id)){
				redirect(base_url('admin/clientes/listar'));
			}
		}
	}
	
	public function listar()
	{
		$data = array();
		$this->load->view('admin/includes/headers', array('titleWebsite' => 'Painel Administrativo - Imobiliária', 'descSite' => 'Aluguel, Temporada, Venda & Arrendamentos'));
		$this->load->view('admin/includes/top', array('titleWebsite' => 'Painel Administrativo - Imobiliária', 'descSite' => 'Aluguel, Temporada, Venda & Arrendamentos'));
		$lista = $this->clientes_model->get(array('status' => 1));
		$this->load->view('admin/clientes-listar', array('lista' => $lista));
		$this->load->view('admin/includes/footer', $data);
	}
	
	public function adicionar()
	{
		$data = array();
		$this->load->view('admin/includes/headers', array('titleWebsite' => 'Painel Administrativo - Imobiliária', 'descSite' => 'Aluguel, Temporada, Venda & Arrendamentos'));
		$this->load->view('admin/includes/top', array('titleWebsite' => 'Painel Administrativo - Imobiliária', 'descSite' => 'Aluguel, Temporada, Venda & Arrendamentos'));
		$data = array('name' => '', 'email' => '', 'password' => '', 'telephone' => '', 'cellphone' => '',
		'address' => '', 'neighborhood' => '', 'city' => '', 'state' => '', 'zipcode' => '', 'idCliente' => 0);
		$this->load->view('admin/clientes-add', $data);
		$this->load->view('admin/includes/footer', $data);
	}
	
	public function editar($id)
	{
		$data = array();
		$this->load->view('admin/includes/headers', array('titleWebsite' => 'Painel Administrativo - Imobiliária', 'descSite' => 'Aluguel, Temporada, Venda & Arrendamentos'));
		$this->load->view('admin/includes/top', array('titleWebsite' => 'Painel Administrativo - Imobiliária', 'descSite' => 'Aluguel, Temporada, Venda & Arrendamentos'));
		$get_by_id = $this->clientes_model->get_by_id($id);
		$customer = $get_by_id[0];
		
		$get_primary_contact = $this->clientes_model->get_primary_contact($id);
		$contact = $get_primary_contact[0];
		
		$data = array('name' => $customer->name, 'email' => $contact->email, 'password' => $contact->password, 'telephone' => $contact->telephone, 'cellphone' => $contact->cellphone, 'address' => $customer->address, 'neighborhood' => $customer->neighborhood, 'zipcode' => $customer->zipcode, 'city' => $customer->city, 'state' => $customer->state, 'idCliente' => $id);
		$this->load->view('admin/clientes-add', $data);
		$this->load->view('admin/includes/footer', $data);
	}
	
	public function salvar($id = null)
	{
		
		if($this->input->post('name') == ''){
			echo '<div class="alert alert-danger">
			  <strong>Erro!</strong> Favor preencha o nome do cliente.
			</div>';
			exit;
		}elseif($this->input->post('address') == ''){
			echo '<div class="alert alert-danger">
			  <strong>Erro!</strong> Favor preencha o endereço do cliente.
			</div>';
			exit;
		}elseif($this->input->post('telephone') == '' && $this->input->post('cellphone') == ''){
			echo '<div class="alert alert-danger">
			  <strong>Erro!</strong> Favor preencha o telefone ou celular do cliente.
			</div>';
			exit;
		}
		
		$data = array(
			'name' => $this->input->post('name'),
			'address' => $this->input->post('address'),
			'neighborhood' => $this->input->post('neighborhood'),
			'extra1' => $this->input->post('extra1'),
			'zipcode' => $this->input->post('zipcode'),
			'city' => $this->input->post('city'),
			'state' => $this->input->post('state'),
			'date_add' => date('Y-m-d H:i:s')
		);
		$idCliente = $this->clientes_model->salvar($data, $id);
		
		$data = array(
			'customer_id' => $idCliente,
			'name' => $this->input->post('name'),
			'email' => $this->input->post('email'),
			'telephone' => $this->input->post('telephone'),
			'cellphone' => $this->input->post('cellphone'),
			'date_add' => date('Y-m-d H:i:s')
		);
		
		$idContato = 0;
		if($id > 0){
			$get_primary_contact = $this->clientes_model->get_primary_contact($id);
			$contact = $get_primary_contact[0];
			$idContato = $contact->id;
		}
		
		$idContato = $this->contatos_model->salvar($data, $idContato);
		
		if($idCliente > 0 && $idContato > 0){
			echo '<div class="alert alert-success">
			  <strong>Cadastrado!</strong> Seu cliente foi cadastrado com sucesso e já pode ser usado.
			</div>';
		}
	}
}

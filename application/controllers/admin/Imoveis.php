<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Imoveis extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
		$this->load->model('clientes_model');
		$this->load->model('imoveis_model');
    }

	public function index()
	{
		
	}
	
	public function salvar_contratos($idImovel = null, $idContrato = null)
	{
		
		foreach($this->input->post() as $k => $v){
			if(substr_count($k, 'price') > 0){
				$data[$k] = str_replace('.', '', $v);
				$data[$k] = str_replace(',', '.', $data[$k]);
			}else{
				$data[$k] = $v;
			}
		}
		
		// Add
		if($idImovel > 0 && $idContrato == null){
			$data['property_id'] = $idImovel;
			$data['date_add'] = date('Y-m-d H:i:s');
			$idContrato = $this->imoveis_model->salvar_contrato($data);
			
			if($idContrato > 0){
				echo '
				<div class="alert alert-success">
				  <strong>Cadastrado!</strong> Contrato cadastrado com sucesso.
				</div>';
			}
		// Edit	
		}elseif($idImovel > 0 && $idContrato > 0){
			$idContrato = $this->imoveis_model->salvar_contrato($data, $idContrato);
			echo '
			<div class="alert alert-success">
			  <strong>Salvo!</strong> Contrato salvo com sucesso.
			</div>';
		}
	}	
	
	public function salvar($idCliente = null, $idImovel = null)
	{
		foreach($this->input->post() as $k => $v){
			if(substr_count($k, 'price_') > 0){
				$data[$k] = str_replace('.', '', $v);
				$data[$k] = str_replace(',', '.', $data[$k]);
			}else{
				$data[$k] = $v;
			}
		}
		
		// Add
		if($idCliente > 0 && $idImovel == null){
			$data['proprietary_id'] = $idCliente;
			$data['date_add'] = date('Y-m-d H:i:s');
			$id_imovel = $this->imoveis_model->salvar($data);
			
			if($id_imovel > 0){
				echo '
				<div class="alert alert-success">
				  <strong>Cadastrado!</strong> Imóvel cadastrado com sucesso.
				</div>';
			}
			
		}elseif($idCliente > 0 && $idImovel > 0){
			$id_imovel = $this->imoveis_model->salvar($data, $idImovel);
			echo '
			<div class="alert alert-success">
			  <strong>Salvo!</strong> Imóvel salvo com sucesso.
			</div>';
		}
	}
	
	public function contratos($idImovel = null, $idProprietario = null, $idLocatario = null)
	{
		$data = array();
		$this->load->view('admin/includes/headers', array('titleWebsite' => 'Painel Administrativo - Imobiliária', 'descSite' => 'Aluguel, Temporada, Venda & Arrendamentos'));
		$this->load->view('admin/includes/top', array('titleWebsite' => 'Painel Administrativo - Imobiliária', 'descSite' => 'Aluguel, Temporada, Venda & Arrendamentos'));
		
		$where['status'] = 'Y';
		
		if($idImovel > 0){
			$where['property_id'] = $idImovel;
		}
		
		if(is_int($idLocatario) && $idLocatario > 0){
			$where['customer_id'] = $idLocatario;
		}
		
		$lista = $this->imoveis_model->contratos_listar($where);
		$this->load->view('admin/imoveis-contratos-listar', array('lista' => $lista));
		$this->load->view('admin/includes/footer', $data);
	}
	
	public function listar($idCliente = null)
	{
		$data = array();
		$this->load->view('admin/includes/headers', array('titleWebsite' => 'Painel Administrativo - Imobiliária', 'descSite' => 'Aluguel, Temporada, Venda & Arrendamentos'));
		$this->load->view('admin/includes/top', array('titleWebsite' => 'Painel Administrativo - Imobiliária', 'descSite' => 'Aluguel, Temporada, Venda & Arrendamentos'));
		
		$where['status'] = 'Y';
		
		if($idCliente > 0){
			$where['proprietary_id'] = $idCliente;
		}
		
		$lista = $this->imoveis_model->listar($where);
		$this->load->view('admin/imoveis-listar', array('lista' => $lista));
		$this->load->view('admin/includes/footer', $data);
	}
	
	public function adicionar($idCliente = null)
	{
		$this->load->view('admin/includes/headers', array('titleWebsite' => 'Painel Administrativo - Imobiliária', 'descSite' => 'Aluguel, Temporada, Venda & Arrendamentos'));
		$this->load->view('admin/includes/top', array('titleWebsite' => 'Painel Administrativo - Imobiliária', 'descSite' => 'Aluguel, Temporada, Venda & Arrendamentos'));
		$nomeCliente = '';
		if($idCliente > 0){
			$cliente = $this->clientes_model->get_by_id($idCliente);
			$nomeCliente = ' imóvel para ' . $cliente[0]->name;
		}
		
		$imoveis = $this->imoveis_model->listar_property_types(array('status' => 'Y', 'property_type >' => '0'));
		$data = array('property_types' => $imoveis, 'name' => '', 'customer_desc' => $nomeCliente, 'idCliente' => $idCliente, 'idImovel' => 0);
		
		$query = $this->db->query("show columns from properties");
		foreach($query->result() as $row){
			$data[$row->Field] = '';
		}
		
		$this->load->view('admin/imoveis-add', $data);
		$this->load->view('admin/includes/footer', $data);
	}
	
	public function editar($idImovel = null)
	{
		$this->load->view('admin/includes/headers', array('titleWebsite' => 'Painel Administrativo - Imobiliária', 'descSite' => 'Aluguel, Temporada, Venda & Arrendamentos'));
		$this->load->view('admin/includes/top', array('titleWebsite' => 'Painel Administrativo - Imobiliária', 'descSite' => 'Aluguel, Temporada, Venda & Arrendamentos'));
		$nomeCliente = '';
		
		$imovel = $this->imoveis_model->listar(array('id' => $idImovel));		
		$imovel = $imovel[0];
		$idCliente = $imovel->proprietary_id;
		
		if($idCliente > 0){
			$cliente = $this->clientes_model->get_by_id($idCliente);
			$nomeCliente = ' imóvel para ' . $cliente[0]->name;
		}
		$imoveis = $this->imoveis_model->listar_property_types(array('status' => 'Y', 'property_type >' => '0'));
		$data = array('property_types' => $imoveis, 'name' => '', 'customer_desc' => $nomeCliente, 'idCliente' => $idCliente, 'idImovel' => $idImovel);
		$query = $this->db->query("show columns from properties");
		
		foreach($query->result() as $row){
			$field = $row->Field;
			$data[$field] = $imovel->$field;
		}
		
		$this->load->view('admin/imoveis-add', $data);
		$this->load->view('admin/includes/footer', $data);
	}
	
	public function excluir($idImovel = null){
		if($idImovel > 0){
			if($this->imoveis_model->excluir($idImovel)){
				redirect(base_url('admin/imoveis/listar'));
			}
		}
	}
	
	public function excluir_contrato($idImovel = null, $idContrato = null){
		if($idImovel > 0){
			if($this->imoveis_model->excluir_contrato($idContrato)){
				redirect(base_url('admin/imoveis/contratos/'.$idImovel));
			}
		}
	}
	
	public function adicionar_contrato($idImovel = null, $idContrato = null)
	{
		$this->session->set_userdata("user_name","João Paulo");
		$this->load->view('admin/includes/headers', array('titleWebsite' => 'Painel Administrativo - Imobiliária', 'descSite' => 'Aluguel, Temporada, Venda & Arrendamentos'));
		$this->load->view('admin/includes/top', array('titleWebsite' => 'Painel Administrativo - Imobiliária', 'descSite' => 'Aluguel, Temporada, Venda & Arrendamentos'));
		# $this->load->view('admin/includes/menu');
		$data = array('customer_id' => '', 'operation' => '', 'date_start' => '', 'date_end' => '', 'date_renew' => '', 'due_at' => '', 'price' => 0, 'condominium' => 0);
		if($idContrato > 0){
			$listar_contratos = $this->imoveis_model->listar_contratos(array('id' => $idContrato));
			$listar_contratos = $listar_contratos[0];
			$data = array('customer_id' => $listar_contratos->customer_id, 'operation' => $listar_contratos->operation, 'date_start' => $listar_contratos->date_start, 'date_end' => $listar_contratos->date_end, 'date_renew' => $listar_contratos->date_renew, 'due_at' => $listar_contratos->due_at, 'price' => $listar_contratos->price, 'condominium' => $listar_contratos->condominium);
		}
		$this->load->view('admin/imoveis-add-contratos', $data);
		$this->load->view('admin/includes/footer', $data);
	}
	
	public function editar_contrato($idImovel = null, $idContrato = null)
	{
		$this->session->set_userdata("user_name","João Paulo");
		$this->load->view('admin/includes/headers', array('titleWebsite' => 'Painel Administrativo - Imobiliária', 'descSite' => 'Aluguel, Temporada, Venda & Arrendamentos'));
		$this->load->view('admin/includes/top', array('titleWebsite' => 'Painel Administrativo - Imobiliária', 'descSite' => 'Aluguel, Temporada, Venda & Arrendamentos'));
		
		$data = array('customer_id' => '', 'operation' => '', 'date_start' => '', 'date_end' => '', 'date_renew' => '', 'due_at' => '', 'price' => '', 'condominium' => '');
		if($idContrato > 0){
			$listar_contratos = $this->imoveis_model->listar_contratos(array('id' => $idContrato));
			$listar_contratos = $listar_contratos[0];
			$data = array('customer_id' => $listar_contratos->customer_id, 'operation' => $listar_contratos->operation, 'date_start' => $listar_contratos->date_start, 'date_end' => $listar_contratos->date_end, 'date_renew' => $listar_contratos->date_renew, 'due_at' => $listar_contratos->due_at, 'price' => $listar_contratos->price, 'condominium' => $listar_contratos->condominium);
		}
		$this->load->view('admin/imoveis-add-contratos', $data);
		$this->load->view('admin/includes/footer', $data);
	}
		
}

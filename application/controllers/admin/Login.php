<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
		$this->load->view('admin/includes/headers', array('titleWebsite' => 'Painel Administrativo - Paladar'));
		$data = array();
		$this->load->view('admin/login', $data);
	}
	
	public function acessar(){
		$this->load->model('usuarios_model');
		$user = $this->input->post('login');
		$pass = $this->input->post('senha');
		if($user == ''){ echo 'Insira um e-mail.'; exit;}
		if($pass == ''){ echo 'Insira uma senha.'; exit;}
		$login_admin = $this->usuarios_model->login_admin(array('status' => 'Y', 'email' => $user, 'senha' => $pass));
		if(count($login_admin) == 1){
			echo 'Login realizado! Você será redirecionado.';
		}else{
			echo 'Dados incorretos.';
		}
	}
}

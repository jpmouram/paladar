<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produtos extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
		$this->load->model('categorias_model');
		$this->load->model('subcategorias_model');
		$this->load->model('produtos_model');
		$this->load->helper(array('form', 'url'));
    }

	public function index()
	{
	}	
	
	function gerar_pdf_produtos($where = array())
	{
		$data = array();
		$this->load->helper('pdf_helper');
		$listar_categorias_subcategorias = $this->produtos_model->listar_categorias_subcategorias();
		$data['categorias'] = $listar_categorias_subcategorias;
		$this->load->view('admin/gerar-catalogo', $data);
	}
	
	public function salvar($id = null)
	{
		foreach($this->input->post() as $k => $v){
			if(substr_count($k, 'valor') > 0){
				$data[$k] = str_replace('.', '', $v);
				$data[$k] = str_replace(',', '.', $data[$k]);
			}else{
				$data[$k] = $v;
			}
		}
		
		// Add
		if($id == null){
			$data['data_add'] = date('Y-m-d H:i:s');
			
			if($this->session->userdata('imagem_produto') && $this->session->userdata('imagem_produto') <> ''){
				$dadosSession = $this->session->userdata('imagem_produto');
				$data['imagem'] = $dadosSession['file_name'];
				$data['caminho_imagem'] = $dadosSession['full_path'];
				$this->session->unset_userdata('imagem_produto');
			}
			
			$id = $this->produtos_model->salvar($data);
			
			if($id > 0){
				echo '
				<div class="alert alert-success">
				  <strong>Cadastrado!</strong> Produto cadastrada com sucesso.
				</div>';
			}
			
		}elseif($id > 0){
			if($this->session->userdata('imagem_produto') <> ''){
				$dadosSession = $this->session->userdata('imagem_produto');
				$data['imagem'] = $dadosSession['file_name'];
				$data['caminho_imagem'] = $dadosSession['full_path'];
				$this->session->unset_userdata('imagem_produto');
			}
			$id_imovel = $this->produtos_model->salvar($data, $id);
			echo '
			<div class="alert alert-success">
			  <strong>Salvo!</strong> Produto salvo com sucesso.
			</div>';
		}
	}
	
	public function listar($id = null)
	{
		$data = array();
		$this->load->view('admin/includes/headers', array('titleWebsite' => 'Painel Administrativo', 'descSite' => ''));
		$this->load->view('admin/includes/top', array('titleWebsite' => 'Painel Administrativo', 'descSite' => ''));
		
		$where = array();
		
		$lista = $this->produtos_model->listar($where);
		$this->load->view('admin/produtos-listar', array('lista' => $lista));
		$this->load->view('admin/includes/footer', $data);
	}
	
	public function adicionar($idCliente = null)
	{
		$this->load->view('admin/includes/headers', array('titleWebsite' => 'Painel Administrativo', 'descSite' => ''));
		$this->load->view('admin/includes/top', array('titleWebsite' => 'Painel Administrativo', 'descSite' => ''));
		
		$data['dados'] = (object) array('produto' => '', 'codigo' => '', 'valor' => 0, 'id_categoria' => '', 'id_sub_categoria' => '');
		
		$data['categorias'] = $this->categorias_model->listar(array('a.id_categoria' => 0));
		
		$this->load->view('admin/produtos-add', $data);
		$this->load->view('admin/includes/footer', $data);
	}
	
	public function upload_imagem($id = null){
		$config['upload_path']          = 'imagens/produtos/';
		$config['allowed_types']        = 'gif|jpg|png';
		$config['max_size']             = 100;
		$config['max_width']            = 1024;
		$config['max_height']           = 768;
		$this->load->library('upload', $config);
		$upload = $this->upload->do_upload('file');
		$this->session->set_userdata('imagem_produto', $this->upload->data());
	}
	
	public function editar($id = null)
	{
		$this->load->view('admin/includes/headers', array('titleWebsite' => 'Painel Administrativo', 'descSite' => ''));
		$this->load->view('admin/includes/top', array('titleWebsite' => 'Painel Administrativo', 'descSite' => ''));
		$dados = $this->produtos_model->listar(array('a.id' => $id));
		$data['categorias'] = $this->categorias_model->listar(array('a.id_categoria' => 0));
		$data['dados'] = $dados[0];
		$data['sub_categorias'] = $this->subcategorias_model->listar(array('a.id_categoria' => $dados[0]->id_categoria));
		$this->load->view('admin/produtos-add', $data);
		$this->load->view('admin/includes/footer', $data);
	}
	
	public function excluir($id = null){
		if($id > 0){
			if($this->produtos_model->excluir($id)){
				redirect(base_url('admin/produtos/listar/'));
			}
		}
	}
		
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subcategorias extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
		$this->load->model('categorias_model');
		$this->load->model('subcategorias_model');
		$this->load->helper(array('form', 'url'));
    }

	public function index()
	{
	}	
	
	public function salvar($id_categoria = null, $id = null)
	{
		foreach($this->input->post() as $k => $v){
			if(substr_count($k, 'price') > 0){
				$data[$k] = str_replace('.', '', $v);
				$data[$k] = str_replace(',', '.', $data[$k]);
			}else{
				$data[$k] = $v;
			}
		}
		
		// Add
		if($id == null){
			$data['data_add'] = date('Y-m-d H:i:s');
			$data['id_categoria'] = $id_categoria;
			
			if($this->session->userdata('imagem_subccategoria') && $this->session->userdata('imagem_subccategoria') <> ''){
				$dadosSession = $this->session->userdata('imagem_subccategoria');
				$data['imagem'] = $dadosSession['file_name'];
				$data['caminho_imagem'] = $dadosSession['full_path'];
				$this->session->unset_userdata('imagem_subccategoria');
			}
			
			$id = $this->categorias_model->salvar($data);
			
			if($id > 0){
				echo '
				<div class="alert alert-success">
				  <strong>Cadastrado!</strong> Categoria cadastrada com sucesso.
				</div>';
			}
			
		}elseif($id > 0){
			if($this->session->userdata('imagem_subccategoria') <> ''){
				$dadosSession = $this->session->userdata('imagem_subccategoria');
				$data['imagem'] = $dadosSession['file_name'];
				$data['caminho_imagem'] = $dadosSession['full_path'];
				$this->session->unset_userdata('imagem_subccategoria');
			}
			$id_imovel = $this->categorias_model->salvar($data, $id);
			echo '
			<div class="alert alert-success">
			  <strong>Salvo!</strong> Categoria salva com sucesso.
			</div>';
		}
	}
	
	public function ajax_listar_options($id = null, $idSubCategoria = null)
	{
		$data = array();
		$where = array();
		if($id > 0 && $id <> ''){
			$where['a.id_categoria'] = $id;
			$lista = $this->subcategorias_model->listar($where);
			if(count($lista) > 0){
				$html = '<option value="">Selecione uma sub categoria</option>';
			}else{
				
				$html = '<option value="">Nenhuma subcategoria foi encontrada.</option>';
			}
			foreach($lista as $row){
				$sel = '';
				if($idSubCategoria == $row->id){$sel = 'selected';}
				$html .= '<option value="'.$row->id.'" '.$sel.'>'.$row->categoria.'</option>';
			}
			echo $html;
		}
	}
	
	public function listar($id = null)
	{
		$data = array();
		$this->load->view('admin/includes/headers', array('titleWebsite' => 'Painel Administrativo', 'descSite' => ''));
		$this->load->view('admin/includes/top', array('titleWebsite' => 'Painel Administrativo', 'descSite' => ''));
		
		$where = array();
		# $where['a.id_categoria'] = '> 0';
		if($id > 0){
			$where['a.id_categoria'] = $id;
		}
		
		$lista = $this->subcategorias_model->listar($where);
		$this->load->view('admin/subcategorias-listar', array('lista' => $lista));
		$this->load->view('admin/includes/footer', $data);
	}
	
	public function adicionar($idCliente = null)
	{
		$this->load->view('admin/includes/headers', array('titleWebsite' => 'Painel Administrativo', 'descSite' => ''));
		$this->load->view('admin/includes/top', array('titleWebsite' => 'Painel Administrativo', 'descSite' => ''));
		
		$query = $this->db->query("show columns from categorias");
		foreach($query->result() as $row){
			if($row->Field <> 'price'){
				$data[$row->Field] = '';
			}else{
				$data[$row->Field] = 0;
			}
		}
		
		$this->load->view('admin/subcategorias-add', $data);
		$this->load->view('admin/includes/footer', $data);
	}
	
	public function upload_imagem($id = null){
		$config['upload_path']          = 'imagens/subcategorias/';
		$config['allowed_types']        = 'gif|jpg|png';
		# $config['max_size']             = 1000;
		# $config['max_width']            = 1024;
		# $config['max_height']           = 768;
		$this->load->library('upload', $config);
		$upload = $this->upload->do_upload('file');
		$this->session->set_userdata('imagem_subccategoria', $this->upload->data());
	}
	
	public function editar($id = null)
	{
		$this->load->view('admin/includes/headers', array('titleWebsite' => 'Painel Administrativo', 'descSite' => ''));
		$this->load->view('admin/includes/top', array('titleWebsite' => 'Painel Administrativo', 'descSite' => ''));
		
		$dados = $this->categorias_model->get_by_id($id);
		$data = $dados[0];
		
		$this->load->view('admin/subcategorias-add', $data);
		$this->load->view('admin/includes/footer', $data);
	}
	
	public function excluir($id = null){
		if($id > 0){
			if($this->categorias_model->excluir($id)){
				redirect(base_url('admin/categorias/listar/'));
			}
		}
	}
		
}

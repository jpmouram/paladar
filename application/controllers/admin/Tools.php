<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tools extends CI_Controller {
	
	public function cep($cep, $output = 0){
		$curl = curl_init();
		// Set some options - we are passing in a useragent too here
		curl_setopt_array($curl, array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => "http://cep.republicavirtual.com.br/web_cep.php?formato=json&cep=" . $cep
		));
		// Send the request & save response to $resp
		$reg = curl_exec($curl);
		# $reg = simplexml_load_string($reg);
		$reg = json_decode($reg);
		// Close request to clear up some resources
		curl_close($curl);
		$dados['sucesso'] = (string) $reg->resultado;
		$dados['rua']     = (string) $reg->tipo_logradouro . ' ' . $reg->logradouro;
		$dados['bairro']  = (string) $reg->bairro;
		$dados['cidade']  = (string) $reg->cidade;
		$dados['estado']  = (string) $reg->uf;
		if($output == 0){
			return json_encode($dados);
		}else{
			echo json_encode($dados);
		}
	}
}

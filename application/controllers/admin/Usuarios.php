<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
		$this->load->model('usuarios_model');
    }

	public function index($id = null)
	{
		$data = array();
		$this->load->view('admin/includes/headers', array('titleWebsite' => 'Painel Administrativo', 'descSite' => ''));
		$this->load->view('admin/includes/top', array('titleWebsite' => 'Painel Administrativo', 'descSite' => ''));
		
		$where = array();
		if($id > 0){
			$where['b.id_categoria'] = $id;
		}
		
		$lista = $this->usuarios_model->listar($where);
		$this->load->view('admin/usuarios-listar', array('lista' => $lista));
		$this->load->view('admin/includes/footer', $data);
	}	
	
	public function salvar($idContrato = null, $idCobranca = null)
	{
		foreach($this->input->post() as $k => $v){
			if(substr_count($k, 'price') > 0){
				$data[$k] = str_replace('.', '', $v);
				$data[$k] = str_replace(',', '.', $data[$k]);
			}else{
				$data[$k] = $v;
			}
		}
		
		// Add
		if($idContrato > 0 && $idCobranca == null){
			$data['contract_id'] = $idContrato;
			$data['date_add'] = date('Y-m-d H:i:s');
			$id_imovel = $this->cobrancas_model->salvar($data);
			
			if($id_imovel > 0){
				echo '
				<div class="alert alert-success">
				  <strong>Cadastrado!</strong> Cobrança cadastrada com sucesso.
				</div>';
			}
			
		}elseif($idContrato > 0 && $idCobranca > 0){
			$id_imovel = $this->cobrancas_model->salvar($data, $idCobranca);
			echo '
			<div class="alert alert-success">
			  <strong>Salvo!</strong> Cobrança salva com sucesso.
			</div>';
		}
	}
	
	public function listar($idContrato = null)
	{
		$data = array();
		$this->load->view('admin/includes/headers', array('titleWebsite' => 'Painel Administrativo - Imobiliária', 'descSite' => 'Aluguel, Temporada, Venda & Arrendamentos'));
		$this->load->view('admin/includes/top', array('titleWebsite' => 'Painel Administrativo - Imobiliária', 'descSite' => 'Aluguel, Temporada, Venda & Arrendamentos'));
		
		$where = array();
		
		if($idContrato > 0){
			$where['contract_id'] = $idContrato;
		}
		
		$lista = $this->cobrancas_model->listar($where);
		
		$this->load->view('admin/cobrancas-listar', array('lista' => $lista));
		$this->load->view('admin/includes/footer', $data);
	}
	
	public function adicionar($idCliente = null)
	{
		$this->load->view('admin/includes/headers', array('titleWebsite' => 'Painel Administrativo - Imobiliária', 'descSite' => 'Aluguel, Temporada, Venda & Arrendamentos'));
		$this->load->view('admin/includes/top', array('titleWebsite' => 'Painel Administrativo - Imobiliária', 'descSite' => 'Aluguel, Temporada, Venda & Arrendamentos'));
		
		$query = $this->db->query("show columns from properties_contracts_charges");
		foreach($query->result() as $row){
			if($row->Field <> 'price'){
				$data[$row->Field] = '';
			}else{
				$data[$row->Field] = 0;
			}
		}
		
		$this->load->view('admin/cobrancas-add', $data);
		$this->load->view('admin/includes/footer', $data);
	}
	
	public function editar($idContrato = null, $idCobranca = null)
	{
		$this->load->view('admin/includes/headers', array('titleWebsite' => 'Painel Administrativo - Imobiliária', 'descSite' => 'Aluguel, Temporada, Venda & Arrendamentos'));
		$this->load->view('admin/includes/top', array('titleWebsite' => 'Painel Administrativo - Imobiliária', 'descSite' => 'Aluguel, Temporada, Venda & Arrendamentos'));
		
		$dados = $this->cobrancas_model->get_by_id($idCobranca);
		$data = $dados[0];
		
		$this->load->view('admin/cobrancas-add', $data);
		$this->load->view('admin/includes/footer', $data);
	}
	
	public function excluir($idContrato = null, $idCobranca = null){
		if($idContrato > 0 && $idCobranca > 0){
			if($this->cobrancas_model->excluir($idCobranca)){
				redirect(base_url('admin/cobrancas/listar/'.$idContrato));
			}
		}
	}
		
}

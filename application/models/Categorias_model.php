<?php

class Categorias_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

	public function salvar($data, $id = null){
		if($id > 0){
			$this->db->where('id', $id);
			$this->db->update('categorias', $data);
			return $id;
		}else{
			$this->db->insert('categorias', $data);
			return $this->db->insert_id();
		}
	}
	
	public function excluir($id = null){
		if($id > 0){
			$this->db->where(array('id' => $id));
			$this->db->update('categorias', array('status' => 0));
			return true;
		}else{
			return false;
		}
	}
	
	public function get_by_id($id){
		$this->db->where('id', $id);
		$query = $this->db->get('categorias');
		return $query->result();
	}
	
	public function listar($data = array()){
		$this->db->select('a.id, a.categoria, count(b.id) as qtd_sub_categorias, group_concat(b.id separator \'||\') as ids, group_concat(b.categoria separator \'||\') as subs');
		$this->db->from('categorias a');
		$this->db->join('categorias b', 'b.id_categoria = a.id and b.status = 1', 'left');
		$this->db->where('a.`status`', 1);
		$this->db->where('a.`id_categoria`', 0);
		$this->db->order_by("a.categoria");
		$this->db->group_by("a.id");
		if($data){
			foreach($data as $w => $v){
				$this->db->where($w, $v);
			}
		}
		$query = $this->db->get();
		return $query->result();
	}
	
}
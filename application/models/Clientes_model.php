<?php

class Clientes_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

	public function salvar($data, $id = null){
		if($id > 0){
			$this->db->where('id', $id);
			$this->db->update('clientes', $data);
			return $id;
		}else{
			$this->db->insert('clientes', $data);
			return $this->db->insert_id();
		}
	}
	
	public function excluir($id = null){
		if($id > 0){
			$this->db->where(array('id' => $id));
			$this->db->update('clientes', array('status' => 0));
			return true;
		}else{
			return false;
		}
	}
	
	public function get_by_id($id){
		$this->db->where('id', $id);
		$query = $this->db->get('clientes');
		return $query->result();
	}
	
	public function get($data){
		if($data){
			foreach($data as $k => $v){
				$this->db->where($k, $v);
			}
		}
		$query = $this->db->get('clientes');
		return $query->result();
	}
}
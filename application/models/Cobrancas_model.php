<?php

class Cobrancas_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

	public function salvar($data, $id = null){
		if($id > 0){
			$this->db->where('id', $id);
			$this->db->update('properties_contracts_charges', $data);
			return $id;
		}else{
			$this->db->insert('properties_contracts_charges', $data);
			return $this->db->insert_id();
		}
	}
	
	public function excluir($id = null){
		if($id > 0){
			$this->db->where(array('id' => $id));
			$this->db->update('properties_contracts_charges', array('status' => 'N'));
			return true;
		}else{
			return false;
		}
	}
	
	public function get_name_payment_method($payment_method){
		if($payment_method == 'cash'){
			return 'Dinheiro';
		}elseif($payment_method == 'credit-card'){
			return 'Cartão de Crédito';
		}elseif($payment_method == 'deposit'){
			return 'Depósito';
		}
	}
	
	public function get_by_id($id){
		$this->db->where('id', $id);
		$query = $this->db->get('properties_contracts_charges');
		return $query->result();
	}
	
	public function listar($data = array()){
		$this->db->select('properties_contracts_charges.id
		, properties_contracts_charges.contract_id
		, properties_contracts_charges.due_at
		, properties_contracts_charges.payment_date
		, properties_contracts_charges.payment_method
		, properties_contracts_charges.price
		, properties_contracts_charges.date_add
		, customers.name as property
		, locatario.name as customer');
		$this->db->from('properties_contracts_charges');
		$this->db->join('properties_contracts', "properties_contracts.id = properties_contracts_charges.contract_id AND properties_contracts.`status` = 'Y'");
		$this->db->join('properties', 'properties.id = properties_contracts.property_id');
		$this->db->join('customers', 'customers.id = properties.proprietary_id');
		$this->db->join('customers locatario', 'locatario.id = properties_contracts.customer_id');
		$this->db->where('properties_contracts_charges.`status`', 'Y');
		if($data){
			foreach($data as $w => $v){
				$this->db->where($w, $v);
			}
		}
		$query = $this->db->get();
		return $query->result();
	}
	
}
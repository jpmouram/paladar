<?php

class Pedidos_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	
	public function listar($data = null){
		if($data <> null){
			$this->db->where($data);
		}
		$this->db->where(array('status' => 1));
		$query = $this->db->get('usuarios');
		return $query->result();
	}
}
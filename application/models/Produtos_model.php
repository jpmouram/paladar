<?php

class Produtos_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

	public function salvar($data, $id = null){
		if($id > 0){
			$this->db->where('id', $id);
			$this->db->update('produtos', $data);
			return $id;
		}else{
			$this->db->insert('produtos', $data);
			return $this->db->insert_id();
		}
	}
	
	public function excluir($id = null){
		if($id > 0){
			$this->db->where(array('id' => $id));
			$this->db->update('produtos', array('status' => 0));
			return true;
		}else{
			return false;
		}
	}
	
	public function get_by_id($id){
		$this->db->where('id', $id);
		$query = $this->db->get('produtos');
		return $query->result();
	}
	
	public function listar($data = array()){
		$this->db->select('a.id, a.codigo, a.caminho_imagem, a.imagem, a.produto, a.descricao, a.valor, a.valor_promocao, b.id as id_sub_categoria, b.categoria as sub_categoria, b.id_categoria, c.categoria, a.embalagem');
		$this->db->from('produtos a');
		$this->db->join('categorias b', 'b.id = a.id_sub_categoria and b.status = 1');
		$this->db->join('categorias c', 'c.id = b.id_categoria and b.status = 1');
		$this->db->where('a.`status`', 1);
		if($data){
			foreach($data as $k => $v){
				$this->db->where($k, $v);
			}
		}
		$this->db->group_by("a.id");
		$query = $this->db->get();
		return $query->result();
	}
	
	public function listar_categorias_subcategorias($data = array()){
		$this->db->select('categorias.categoria, GROUP_CONCAT(DISTINCT(CONCAT(sub.id, \':\', sub.categoria, \':\', sub.imagem))) as sub_categoria');
		$this->db->from('produtos a');
		$this->db->join('categorias sub', 'sub.id = a.id_sub_categoria and sub.`status` = 1');
		$this->db->join('categorias', 'categorias.id = sub.id_categoria and categorias.`status` = 1');
		$this->db->where('a.`status`', 1);
		if($data){
			foreach($data as $k => $v){
				$this->db->where($k, $v);
			}
		}
		$this->db->group_by("categorias.id");
		$query = $this->db->get();
		return $query->result();		
	}
	
}
<?php

class Seguranca_Clientes_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
		if(!$this->session->userdata('session_cliente')){
			redirect(base_url('clientes/cadastro'));
		}
    }
}
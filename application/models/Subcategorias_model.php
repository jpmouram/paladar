<?php

class Subcategorias_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

	public function salvar($data, $id = null){
		if($id > 0){
			$this->db->where('id', $id);
			$this->db->update('categorias', $data);
			return $id;
		}else{
			$this->db->insert('categorias', $data);
			return $this->db->insert_id();
		}
	}
	
	public function excluir($id = null){
		if($id > 0){
			$this->db->where(array('id' => $id));
			$this->db->update('categorias', array('status' => 0));
			return true;
		}else{
			return false;
		}
	}
	
	public function get_by_id($id){
		$this->db->where('id', $id);
		$query = $this->db->get('categorias');
		return $query->result();
	}
	
	public function listar($data = array()){
		$this->db->select('a.id, a.categoria, a.id_categoria, b.categoria as nome_categoria');
		$this->db->from('categorias a');
		$this->db->join('categorias b', 'b.id = a.id_categoria and b.status = 1');
		$this->db->where('a.`status`', 1);
		$this->db->order_by("a.categoria");
		$this->db->group_by("a.id");
		if($data){
			foreach($data as $w => $v){
				$this->db->where($w, $v);
			}
		}
		$query = $this->db->get();
		return $query->result();
	}
	
}
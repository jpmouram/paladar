<?php @session_start();
tcpdf();

class MYPDF extends TCPDF {

    //Page header
    public function Header() {
        // Logo
        $image_file = K_PATH_IMAGES.PDF_HEADER_LOGO; # 'logo_example.jpg';
        $image = '../../assets/global/img/Nomad_Logo_moyen.png';
        # $this->Image($image_file, 10, 10, 15, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        // Set font
        # $this->SetFont('helvetica', 'B', 20);
        // Title
        $content = '<table width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td><img src="'.base_url('assets/adm/logo-paladar-1.jpg').'"></td>
			<td align="right" width="50%">
			
			
			<table width="200" cellpadding="0" cellspacing="0" style="float:right;">
			<tr>
				<td height="60"></td>
			</tr>
			<tr>
				<td align="right" style="color:#27186b; font-size:20px; font-weight:bold; border-bottom:3px solid red;">PANIFICAÇÃO</td>
			</tr>
			</table>
			
			</td>
		</tr>
		</table>';
		$this->writeHTML($content, true, false, true, false, '');
    }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-22);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        # $this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
		$content = 'Footer';
		# $this->writeHTML($content, true, false, true, false, 'C');
    }
}

$obj_pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$obj_pdf->SetCreator(PDF_CREATOR);
$title = "Catálogo";
$obj_pdf->SetTitle($title);
$obj_pdf->SetHeaderData(PDF_HEADER_LOGO, 20, 'Catálogo');
$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
# $obj_pdf->SetDefaultMonospacedFont('helvetica');
$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$obj_pdf->SetFooterMargin(50);
$obj_pdf->SetMargins(15, 55, 15);
$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
# $obj_pdf->SetFont('helvetica', '', 9);
$obj_pdf->setFontSubsetting(false);
$obj_pdf->SetPrintHeader(false);
$obj_pdf->AddPage();

// Capa catalogo

$obj_pdf->writeHTML('<img src="'.APPPATH.'/../assets/adm/capa-catalogo.jpg">', true, false, true, false, '');
$obj_pdf->SetPrintHeader();
foreach($categorias as $ca){
	$html_sub_categoria = '';
	$ex = explode(',', $ca->sub_categoria);
	if(count($ex) > 0){
		foreach($ex as $sc){
			$e = explode(':', $sc);
			$img = '';
			if($e[2] <> '' && file_exists(APPPATH.'/../imagens/subcategorias/'.$e[2])){
			$img = '<img width="100" src="'.base_url('imagens/subcategorias/'.$e[2]).'">';}
			$html_sub_categoria .= '
			
			<table width="100%" cellpadding="3" cellspacing="2" style="font-family:cera; font-size:10px; color:#333333;">
			<tr>
				<td width="20%">'.$img.'</td>
				<td width="80%">
					<table width="100%" cellpadding="3" cellspacing="2" style="font-family:cera; font-size:10px; color:#333333; border:1px solid #CECECE;">
					<tr>
						<td align="center" bgcolor="#F4F3F2"><strong>'.$e[1].'</strong></td>
					</tr>
					</table>
				';
			
			// Produtos
			$produtos = $this->produtos_model->listar(array('id_sub_categoria' => $e[0]));
			$html_sub_categoria .= '
			
			<table width="100%" cellpadding="3" cellspacing="2" style="font-family:cera; font-size:10px; color:#333333; border:1px solid #CECECE;">
			<tr>
				<!--<td align="center"><strong></strong></td>-->
				<td align="center"><strong>Código</strong></td>
				<td align="center"><strong>Produto</strong></td>
				<td align="center"><strong>Embalagem</strong></td>
				<td align="center"><strong>Valor</strong></td>
			</tr>
			</table>';
			foreach($produtos as $prod){
				
				$img = '';
				if($e[2] <> '' && file_exists(APPPATH.'/../imagens/produtos/'.$prod->imagem)){$img='<img width="40" src="'.base_url('imagens/produtos/'.$prod->imagem).'">';}
				
				$html_sub_categoria .= '<table width="100%" cellpadding="3" cellspacing="2" style="font-family:cera; font-size:10px; color:#333333; border:1px solid #CECECE;">
				<tr>
					<!--<td align="center">'.$img.'</td>-->
					<td align="center">'.$prod->codigo.'</td>
					<td align="center">'.$prod->produto.'</td>
					<td align="center">'.$prod->embalagem.'</td>
					<td align="center">'.number_format($prod->valor,2,",",".").'</td>
				</tr>
				</table>';
			}
			$html_sub_categoria .= '<br><br></td>
			</tr>
			</table>';
		}
	}else{
		// Apenas uma sub-categoria
		$e = explode(':', $ca->sub_categoria);
		$html_sub_categoria = '<table width="100%" cellpadding="3" cellspacing="2" style="font-family:cera; font-size:10px; color:#333333; border:1px solid #CECECE;">
		<tr>
			<td align="center" bgcolor="#F4F3F2"><strong>'.$ca->categoria.' - '.$e[1].'</strong></td>
		</tr>
		</table>';
	}
	
	$content = '<table width="100%" cellpadding="3" cellspacing="2" style="font-family:cera; font-size:16px; color:#333333;">
	<tr>
		<td align="center" bgcolor="#F4F3F2"><strong>'.$ca->categoria.'</strong></td>
	</tr>
	</table>
	' . $html_sub_categoria;
	for($i=1; $i<=10; $i++){
		$obj_pdf->writeHTML($content, true, false, true, false, '');
	}
}

/*
if(!is_dir(getcwd().'/catalagos')){
	mkdir(getcwd().'/catalagos', 0777);
}

if(!is_dir(getcwd().'/catalagos/'.date('Y'))){
	mkdir(getcwd().'/catalagos/'.date('Y'), 0777);
}

if(!is_dir(getcwd().'/catalagos/'.date('Y').'/'.date('m'))){
	mkdir(getcwd().'/catalagos/'.date('Y').'/'.date('m'), 0777);
}

if(!is_dir(getcwd().'/catalagos/'.date('Y').'/'.date('m').'/'.date('d'))){
	mkdir(getcwd().'/catalagos/'.date('Y').'/'.date('m').'/'.date('d'), 0777);
}
$obj_pdf->Output(getcwd().'/catalagos/'.date('Y').'/'.date('m').'/'.date('d').'/catalago-'.time().'.pdf', 'FI');
*/

$obj_pdf->Output(getcwd().'/catalagos/catalago.pdf', 'I');
?>
<!-- footer content -->
        <footer>
          <div class="pull-right" style="display:none;">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>
	
    <!-- Bootstrap -->
    <script src="<?php echo base_url('assets/gentelella/vendors/bootstrap/dist/js/bootstrap.min.js');?>"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url('assets/gentelella/vendors/fastclick/lib/fastclick.js');?>"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url('assets/gentelella/vendors/nprogress/nprogress.js');?>"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url('assets/gentelella/vendors/iCheck/icheck.min.js');?>"></script>
    <!-- Datatables -->
    <script src="<?php echo base_url('assets/gentelella/vendors/datatables.net/js/jquery.dataTables.min.js');?>"></script>
    <script src="<?php echo base_url('assets/gentelella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js');?>"></script>
    <script src="<?php echo base_url('assets/gentelella/vendors/datatables.net-buttons/js/dataTables.buttons.min.js');?>"></script>
    <script src="<?php echo base_url('assets/gentelella/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js');?>"></script>
    <script src="<?php echo base_url('assets/gentelella/vendors/datatables.net-buttons/js/buttons.flash.min.js');?>"></script>
    <script src="<?php echo base_url('assets/gentelella/vendors/datatables.net-buttons/js/buttons.html5.min.js');?>"></script>
    <script src="<?php echo base_url('assets/gentelella/vendors/datatables.net-buttons/js/buttons.print.min.js');?>"></script>
    <script src="<?php echo base_url('assets/gentelella/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js');?>"></script>
    <script src="<?php echo base_url('assets/gentelella/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js');?>"></script>
    <script src="<?php echo base_url('assets/gentelella/vendors/datatables.net-responsive/js/dataTables.responsive.min.js');?>"></script>
    <script src="<?php echo base_url('assets/gentelella/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js');?>"></script>
    <script src="<?php echo base_url('assets/gentelella/vendors/datatables.net-scroller/js/dataTables.scroller.min.js');?>"></script>
    <script src="<?php echo base_url('assets/gentelella/vendors/jszip/dist/jszip.min.js');?>"></script>
    <script src="<?php echo base_url('assets/gentelella/vendors/pdfmake/build/pdfmake.min.js');?>"></script>
    <script src="<?php echo base_url('assets/gentelella/vendors/pdfmake/build/vfs_fonts.js');?>"></script>
    <script src="<?php echo base_url('assets/gentelella/vendors/dropzone/dist/min/dropzone.min.js');?>"></script>
    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url('assets/gentelella/build/js/custom.min.js');?>"></script>
	
	<script src="<?php echo base_url('assets/js/jquery.maskMoney.js');?>"></script>
	<script src="<?php echo base_url('assets/js/jquery.mask.js');?>"></script>
	
	<script>

	$(document).ready(function()
	{
		$('.zipcode').mask('00000-000');
		$('.phone').mask('(00) 0000-00000');
		
		 $(".moeda2").maskMoney({
			 prefix: "",
			 decimal: ",",
			 thousands: "."
		 });
	});
	
	function buscarSubCategorias(idCategoria, idSubCategoria, campoRetorno){
		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>admin/subcategorias/ajax_listar_options/"+idCategoria+"/"+idSubCategoria,
			success: function(data){
				console.log(data);
				$(campoRetorno).html(data);
			}
		});
	}

	function buscarCep(cep, campoEndereco, campoBairro, campoCidade, campoEstado){
		$(function(){
			$.ajax({
				type: "POST",
				url: "<?php echo base_url('admin/tools/cep/');?>" + cep.replace('-', '') + "/1",
				dataType: 'json',
				success: function(data){
					if(data.cidade != ''){
						$(campoEndereco).val(data.rua);
						$(campoBairro).val(data.bairro);
						$(campoCidade).val(data.cidade);
						$(campoEstado).val(data.estado);
					}
				}
			});
		})
	}
	</script>

  </body>
</html>
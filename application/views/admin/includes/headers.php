<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $titleWebsite;?></title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url('assets/gentelella/vendors/bootstrap/dist/css/bootstrap.min.css');?>" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url('assets/gentelella/vendors/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url('assets/gentelella/vendors/nprogress/nprogress.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/gentelella/vendors/animate.css/animate.min.css');?>" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?php echo base_url('assets/gentelella/vendors/iCheck/skins/flat/green.css');?>" rel="stylesheet">
    <!-- Datatables -->
    <link href="<?php echo base_url('assets/gentelella/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/gentelella/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/gentelella/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/gentelella/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/gentelella/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/gentelella/vendors/dropzone/dist/min/dropzone.min.css');?>" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo base_url('assets/gentelella/build/css/custom.min.css');?>" rel="stylesheet">
	
	<!-- jQuery -->
    <script src="<?php echo base_url('assets/gentelella/vendors/jquery/dist/jquery.min.js');?>"></script>
    <script src="<?php echo base_url('assets/gentelella/vendors/fastclick/lib/fastclick.js');?>"></script>
	
	<style>
	.site_title{font-size:13px;}
	.loader {
	  border: 2px solid #f3f3f3;
	  border-radius: 50%;
	  border-top: 2px solid #3498db;
	  width: 20px;
	  height: 20px;
	  -webkit-animation: spin 2s linear infinite;
	  animation: spin 2s linear infinite;
	}
	@keyframes spin {
		0% { transform: rotate(0deg); }
		100% { transform: rotate(360deg); }
	}
	
	.dz-preview{float:left; margin-right:10px;}
	.dz-success-mark, .dz-error-mark{display:none;}
	</style>
  </head>
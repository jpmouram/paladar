<body class="nav-md">
<div class="container body">
<div class="main_container">
<div class="col-md-3 left_col">
  <div class="left_col scroll-view">
	<div class="navbar nav_title" style="border: 0;">
	  <a href="index.html" class="site_title"><!--<i class="fa fa-paw"></i> --><span><?php echo $titleWebsite;?></span></a>
	</div>

	<div class="clearfix"></div>

	<!-- menu profile quick info -- >
	<div class="profile clearfix">
	  <div class="profile_pic">
		<img src="<?php echo base_url('images/img.jpg');?>" alt="..." class="img-circle profile_img">
	  </div>
	  <div class="profile_info">
		<span>Bem vindo,</span>
		<h2><?php echo $this->session->userdata('user_name');?></h2>
	  </div>
	</div>
	<!-- /menu profile quick info -->
	<br />

	<!-- sidebar menu -->
	<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
	  
	  <div class="menu_section">
		<h3>Menu Principal</h3>
		<ul class="nav side-menu">
		  <li><a href="./"><i class="fa fa-home"></i> Home</a></li>
		  
		  <li><a><i class="fa fa-table"></i> Categorias <span class="fa fa-chevron-down"></span></a>
			<ul class="nav child_menu">
			  <li><a href="<?php echo base_url('admin/categorias/listar');?>">Listar Categorias</a></li>
			  <li><a href="<?php echo base_url('admin/categorias/adicionar');?>">Adicionar Categoria</a></li>
			  <li><a href="<?php echo base_url('admin/subcategorias/listar');?>">Sub Categorias</a></li>
			</ul>
		  </li>		  
		  <li><a><i class="fa fa-clone"></i>Produtos <span class="fa fa-chevron-down"></span></a>
			<ul class="nav child_menu">
			  <li><a href="<?php echo base_url('admin/produtos/listar');?>">Listar Produtos</a></li>
			  <li><a href="<?php echo base_url('admin/produtos/adicionar');?>">Adicionar Produto</a></li>
			  <li><a href="<?php echo base_url('admin/produtos/gerar_pdf_produtos');?>" target="_blank">Gerar Catálogo</a></li>
			</ul>
		  </li>
		  <li><a><i class="fa fa-user"></i> Clientes <span class="fa fa-chevron-down"></span></a>
			<ul class="nav child_menu">
			  <li><a href="<?php echo base_url('admin/clientes/listar');?>">Listar Clientes</a></li>
			  <li><a href="<?php echo base_url('admin/clientes/adicionar');?>">Adicionar Cliente</a></li>
			</ul>
		  </li>
		  <li><a><i class="fa fa-bar-chart-o"></i> Relatórios <span class="fa fa-chevron-down"></span></a>
			<ul class="nav child_menu">
			  <li><a href="#">Por Produto</a></li>
			  <li><a href="#">Por Cliente</a></li>
			  <li><a href="#">Por Período</a></li>
			</ul>
		  </li>
		</ul>
	  </div>
	  
	  <div class="menu_section">
		<h3>Configuraçoes</h3>
		<ul class="nav side-menu">
		  <li><a href="#"><i class="fa fa-users"></i> Usuários <span class="fa fa-chevron-down"></span></a>
		</ul>
	  </div>

	</div>
	<!-- /sidebar menu -->

	<!-- /menu footer buttons 
	<div class="sidebar-footer hidden-small">
	  <a data-toggle="tooltip" data-placement="top" title="Settings">
		<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
	  </a>
	  <a data-toggle="tooltip" data-placement="top" title="FullScreen">
		<span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
	  </a>
	  <a data-toggle="tooltip" data-placement="top" title="Lock">
		<span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
	  </a>
	  <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
		<span class="glyphicon glyphicon-off" aria-hidden="true"></span>
	  </a>
	</div>-->
	<!-- /menu footer buttons -->
  </div>
</div>

<!-- top navigation -->
<div class="top_nav">
  <div class="nav_menu">
	<nav>
	  <div class="nav toggle">
		<a id="menu_toggle"><i class="fa fa-bars"></i></a>
	  </div>

	  <ul class="nav navbar-nav navbar-right" style="display:none;">
		<li class="">
		  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
			<img src="<?php echo base_url('images/img.jpg');?>" alt=""><?php echo $this->session->userdata('user_name');?>
			<span class=" fa fa-angle-down"></span>
		  </a>
		  <ul class="dropdown-menu dropdown-usermenu pull-right">
			<li><a href="javascript:;"> Meus Dados</a></li>
			<li>
			  <a href="javascript:;">
				<span class="badge bg-red pull-right">12</span>
				<span>Atividades Pendentes</span>
			  </a>
			</li>
			<li><a href="javascript:;">Suporte Técnico</a></li>
			<li><a href="#"><i class="fa fa-sign-out pull-right"></i> Sair</a></li>
		  </ul>
		</li>

		<li role="presentation" class="dropdown">
		  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
			<i class="fa fa-envelope-o"></i>
			<span class="badge bg-green">6</span>
		  </a>
		  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
			<li>
			  <a>
				<span class="image"><img src="<?php echo base_url('images/img.jpg');?>" alt="Profile Image" /></span>
				<span>
				  <span>John Smith</span>
				  <span class="time">3 mins ago</span>
				</span>
				<span class="message">
				  Film festivals used to be do-or-die moments for movie makers. They were where...
				</span>
			  </a>
			</li>
			<li>
			  <a>
				<span class="image"><img src="<?php echo base_url('images/img.jpg');?>" alt="Profile Image" /></span>
				<span>
				  <span>John Smith</span>
				  <span class="time">3 mins ago</span>
				</span>
				<span class="message">
				  Film festivals used to be do-or-die moments for movie makers. They were where...
				</span>
			  </a>
			</li>
			<li>
			  <a>
				<span class="image"><img src="<?php echo base_url('images/img.jpg');?>" alt="Profile Image" /></span>
				<span>
				  <span>John Smith</span>
				  <span class="time">3 mins ago</span>
				</span>
				<span class="message">
				  Film festivals used to be do-or-die moments for movie makers. They were where...
				</span>
			  </a>
			</li>
			<li>
			  <a>
				<span class="image"><img src="<?php echo base_url('images/img.jpg');?>" alt="Profile Image" /></span>
				<span>
				  <span>John Smith</span>
				  <span class="time">3 mins ago</span>
				</span>
				<span class="message">
				  Film festivals used to be do-or-die moments for movie makers. They were where...
				</span>
			  </a>
			</li>
			<li>
			  <div class="text-center">
				<a>
				  <strong>See All Alerts</strong>
				  <i class="fa fa-angle-right"></i>
				</a>
			  </div>
			</li>
		  </ul>
		</li>
	  </ul>
	</nav>
  </div>
</div>
<!-- /top navigation -->
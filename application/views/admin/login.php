﻿<body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <form method="post" id='formLogin'>
              <h1>Login Administrador</h1>
              <div>
                <input type="text" id="userLogin" class="form-control" placeholder="E-mail" required="" />
              </div>
              <div>
                <input type="password" id="passLogin" class="form-control" placeholder="Senha" required="" />
              </div>
              <div>
                <button id="sendLogin" class="btn btn-default submit" type="submit">Entrar</button>
                <a href="#signup" class="to_register">Esqueceu sua senha?</a>
              </div>

              <div class="clearfix"></div>

              <div class="separator">              
                <div class="clearfix"></div>
              </div>
			  
			  <div id="retornoLogin"></div>
			  
            </form>
          </section>
        </div>

        <div id="register" class="animate form registration_form">
          <section class="login_content">
            <form>
              <h1>Esqueceu sua senha?</h1>
              <div>
                <input type="email" class="form-control" placeholder="Email" required="" />
              </div>
              <div>
                <a class="btn btn-default submit" href="index.html">Reenviar Senha</a>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">Já é um mebro ?
                  <a href="#signin" class="to_register"> Log in </a>
                </p>

                <div class="clearfix"></div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
  </body>
  </html>
  
  <script>
  $(function(){
	  $('#formLogin').submit(function(){
		  $.ajax({
			  type: "POST",
			  url: "<?php echo base_url('admin/login/acessar');?>",
			  data: {login: $('#userLogin').val(), senha: $('#passLogin').val()},
			  success:function(data){
					$('#retornoLogin').html(data);
					if(data.match('Login realizado! Você será redirecionado.')){
						window.location.href = "<?php echo base_url('admin/usuarios');?>";
					}
			  }
		  });
		  return false;
	  });
  })
  </script>
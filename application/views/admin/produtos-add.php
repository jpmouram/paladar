<script type="text/javascript">
$(function(){
	$('#form_cad_client').submit(function(){
		$("#txtRetornoCadCliente").html('<div class="loader" style="position:relative; top:10px;"></div>');
		$.ajax({
			type: "POST",
			url: "<?php echo base_url('admin/produtos/salvar/'.$this->uri->segment(4));?>",
			data: {produto: $('#produto').val(), id_sub_categoria: $('#id_sub_categoria').val(), codigo: $('#codigo').val(), valor: $('#valor').val()},
			success: function(data){
				$("#txtRetornoCadCliente").html(data);
				if(data.match('success')){
					/*setTimeout(function(){
						window.location.href = '<?php echo base_url('admin/imoveis/listar');?>';
					}, 1200);
					*/
				}
			}
		});
		return false;
	});
})
</script>

<!-- page content -->
<div class="right_col" role="main">
  <div class="">
	<div class="page-title">
	  <div class="title_left">
		<h3>Produtos</h3>
	  </div>
	</div>

	<div class="clearfix"></div>

	<div class="row">
	  <div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
		  <div class="x_title">
			<h2>Gerenciando Produtos</h2>
			<ul class="nav navbar-right panel_toolbox">
			  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
			  </li>
			</ul>
			<div class="clearfix"></div>
		  </div>
		  <div class="x_content">
			<form method="post" id="form_cad_client">
				<div class="col-sm-12">
					<div class="form-group">
					  <label for="produto">Nome do Produto:</label>
					  <input type="text" class="form-control" required id="produto" value="<?php echo $dados->produto;?>" placeholder="Nome do Produto" name="produto">
					</div>
					<div class="form-group">
					  <label for="categoria">Categoria do Produto:</label>
					  <select class="form-control" required id="categoria" name="categoria" onChange="javascript:buscarSubCategorias(this.value, '<?php echo $dados->id_sub_categoria;?>', '#id_sub_categoria');">
					  <option value="">Categoria do Produto</option>
					  <?php foreach($categorias as $row){ ?>
						<option value="<?php echo $row->id;?>" <?php if($row->id == $dados->id_categoria){ echo 'selected';}?>><?php echo $row->categoria;?></option>
					  <?php } ?>
					  </select>
					</div>
					<div class="form-group">
					  <label for="id_sub_categoria">Sub categoria do Produto:</label>
					  <select class="form-control" required id="id_sub_categoria" name="id_sub_categoria">
					  <option value="">Sub categoria do Produto</option>
					  <?php if($sub_categorias){?>
					  <?php foreach($sub_categorias as $row){?>
						<option value="<?php echo $row->id ?>" <?php if($row->id == $dados->id_sub_categoria){echo 'selected';} ?>><?php echo $row->categoria ?></option>
					  <?php } ?>
					  <?php } ?>
					  </select>
					</div>
					<div class="form-group">
					  <label for="codigo">Código do Produto:</label>
					  <input type="text" class="form-control" required id="codigo" value="<?php echo $dados->codigo;?>" placeholder="Código do Produto" name="codigo">
					</div>
					<div class="form-group">
					  <label for="valor">Valor do Produto:</label>
					  <input type="text" class="form-control moeda2" required id="valor" value="<?php echo number_format($dados->valor,2,",",".");?>" placeholder="Valor do Produto" name="valor">
					</div>
					<div class="col-sm-12" id="imagem_subcategoria"></div>
					<div class="form-group">
						<button type="button" class="btn btn-default triggerDz">Carregar Imagem</button>
					</div>
					<div class="form-group">
						<?php if($dados->caminho_imagem){ ?>
						<a href="<?php echo base_url('imagens/produtos/'.$dados->imagem);?>" target="_blank"><button type="button" class="btn btn-default" title="<?php echo $dados->caminho_imagem;?>">Imagem Atual</button></a>
						<?php } ?>
					</div>
					<?php if($dados->caminho_imagem){ ?>
					<div class="form-group">
						<img src="<?php echo base_url('imagens/produtos/'.$dados->imagem);?>" width="100">
					</div>
					<?php } ?>
				</div>
				<div class="col-sm-12">
					<div class="form-group">
						<button type="submit" class="btn btn-default">Salvar</button>
					</div>
				</div>
				<div class="col-sm-12" id="txtRetornoCadCliente" style="margin-top:10px;"></div>
			  </form>
		  </div>
		</div>
	  </div>
	  
	</div>
  </div>
</div>
<!-- /page content -->

<script>
$(function(){
	$("div#imagem_subcategoria").dropzone({ url: "<?php echo base_url('admin/produtos/upload_imagem');?>", clickable: ['.triggerDz'], maxFiles: 1});
})
</script>
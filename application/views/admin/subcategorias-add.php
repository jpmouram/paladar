<script type="text/javascript">
$(function(){
	$('#form_cad_client').submit(function(){
		$("#txtRetornoCadCliente").html('<div class="loader" style="position:relative; top:10px;"></div>');
		$.ajax({
			type: "POST",
			url: "<?php echo base_url('admin/subcategorias/salvar/'.$this->uri->segment(4).'/'.$this->uri->segment(5));?>",
			data: {categoria: $('#categoria').val()},
			success: function(data){
				$("#txtRetornoCadCliente").html(data);
				if(data.match('success')){
					/*setTimeout(function(){
						window.location.href = '<?php echo base_url('admin/imoveis/listar');?>';
					}, 1200);
					*/
				}
			}
		});
		return false;
	});
})
</script>

<!-- page content -->
<div class="right_col" role="main">
  <div class="">
	<div class="page-title">
	  <div class="title_left">
		<h3>Sub Categorias</h3>
	  </div>
	</div>

	<div class="clearfix"></div>

	<div class="row">
	  <div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
		  <div class="x_title">
			<h2>Gerenciando Sub Categorias</h2>
			<ul class="nav navbar-right panel_toolbox">
			  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
			  </li>
			  <!--
			  <li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
				<ul class="dropdown-menu" role="menu">
				  <li><a href="#">Settings 1</a>
				  </li>
				  <li><a href="#">Settings 2</a>
				  </li>
				</ul>
			  </li>
			  <li><a class="close-link"><i class="fa fa-close"></i></a>
			  </li>-->
			</ul>
			<div class="clearfix"></div>
		  </div>
		  <div class="x_content">
			<form method="post" id="form_cad_client">
				<div class="col-sm-12">
					<div class="form-group">
					  <label for="price">Nome da Categoria:</label>
					  <input type="text" class="form-control" required id="categoria" value="<?php echo $categoria;?>" placeholder="Nome da Categoria" name="categoria">
					</div>
				</div>
				<div class="col-sm-12" id="imagem_subcategoria"></div>
				<div class="col-sm-12">
					<div class="form-group">
						<button type="button" class="btn btn-default triggerDz">Carregar Imagem</button>
						<?php if($caminho_imagem){ ?>
						<a href="<?php echo base_url('imagens/subcategorias/'.$imagem);?>" target="_blank"><button type="button" class="btn btn-default" title="<?php echo $caminho_imagem;?>">Imagem Atual</button></a>
						<?php } ?>
						<button type="submit" class="btn btn-default">Salvar</button>
					</div>
					<?php if($caminho_imagem){ ?>
					<img src="<?php echo base_url('imagens/subcategorias/'.$imagem);?>" width="100">
					<?php } ?>
				</div>
				<div class="col-sm-12" id="txtRetornoCadCliente" style="margin-top:10px;"></div>
			  </form>
		  </div>
		</div>
	  </div>
	  
	</div>
  </div>
</div>
<!-- /page content -->	

<script>
$(function(){
	$("div#imagem_subcategoria").dropzone({ url: "<?php echo base_url('admin/subcategorias/upload_imagem');?>", clickable: ['.triggerDz'], maxFiles: 1});
})
</script>
<!-- page content -->
<div class="right_col" role="main">
  <div class="">
	<div class="page-title">
	  <div class="title_left">
		<h3>Sub Categorias <small> - Listar Sub Categorias</small></h3>
	  </div>
	  <?php /*
	  <div class="title_right">
		<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
		  <div class="input-group">
			<input type="text" class="form-control" placeholder="Search for...">
			<span class="input-group-btn">
			  <button class="btn btn-default" type="button">Go!</button>
			</span>
		  </div>
		</div>
	  </div>
	  */ ?>
	</div>

	<div class="clearfix"></div>

	<div class="row">
	  <div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
		  <div class="x_title">
			<h2>Sub Categorias <small>Todos as sub categorias cadastradas</small></h2>
			<ul class="nav navbar-right panel_toolbox">
			  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
			  </li>
			  <!--
			  <li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
				<ul class="dropdown-menu" role="menu">
				  <li><a href="#">Settings 1</a>
				  </li>
				  <li><a href="#">Settings 2</a>
				  </li>
				</ul>
			  </li>
			  <li><a class="close-link"><i class="fa fa-close"></i></a>
			  </li>-->
			</ul>
			<div class="clearfix"></div>
		  </div>
		  <div class="x_content">
			<?php
			
			if($this->uri->segment(4) > 0){
				echo '<p class="text-muted font-13 m-b-30"><a href="'.base_url('admin/subcategorias/adicionar/'.$this->uri->segment(4)).'"><button type="button" class="btn btn-round btn-info btn-sm">Cadastrar Novo</button></a></p>';
			}
			
			/*
			<p class="text-muted font-13 m-b-30">
			  DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code>
			</p>
			*/ ?>
			<table id="datatable" class="table table-striped table-bordered">
			  <thead>
				<tr>
				  <th class="col-sm-6">Sub Categoria</th>
				  <th class="col-sm-5">Categoria</th>
				  <th>Ações</th>
				</tr>
			  </thead>

			  <tbody>
				<?php foreach ($lista as $row){?>
				  <tr>
					<td><?php echo $row->categoria;?></td>
					<td><?php echo $row->nome_categoria;?></td>
					<td><a title="Editar" href="<?php echo base_url('admin/subcategorias/editar/'.$row->id.'/'.$row->id);?>"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></a> <a onClick="javascript:var msg = window.confirm('Deseja realmente remover o registro ?'); if(msg == true){ return true;}else{return false;}" href="<?php echo base_url('admin/subcategorias/excluir/'.$row->id);?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>
				  </tr>
				<?php } ?>
			  </tbody>
			</table>
		  </div>
		</div>
	  </div>
	  
	</div>
  </div>
</div>
<!-- /page content -->	
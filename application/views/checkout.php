<div class="w3l_banner_nav_right">
<!-- about -->
		<div class="privacy about">
			<h3><span>Carrinho de Compras</span></h3>
			
	      <div class="checkout-right">
				<table class="timetable_sub">
					<thead>
						<tr>
							<th>Item.</th>	
							<th>Qtd</th>
							<th>Produto</th>
							<th>Valor</th>
							<th>Remover</th>
						</tr>
					</thead>
					<tbody id="html_produtos">
					<?php 
					$c=0;
					$subtotal1=0;
					$subtotal2=0;
					foreach($_SESSION['carrinho'] as $k => $v){
						$id = $k;
						$v['imagem'] = '';
						$c++;
						$subtotal1 += $v['valor_unit'];
						$subtotal2 += ($v['valor_unit'] * $v['qtd']);
						echo '
						<tr class="rem'.$c.'">
							<td class="invert">'.$c.'</td>
							<td class="invert">
								 <div class="entry value"><span>'.$v['qtd'].'</span></div>
							</td>
							<td class="invert"><a href="'.base_url('produtos/visualizar/'.$id.'/'.$v['nome']).'">'.$v['nome'].'</a></td>
							
							<td class="invert">'.number_format($v['valor_unit'], 2,",",".").'</td>
							<td class="invert" align="center">
								<div class="rem">
									<a href="'.base_url('carrinho/remover/'.$id).'"><div class="close1" style="position:unset; margin:0 auto;"> </div></a>
								</div>

							</td>
						</tr>';
					}
					
					echo '<tr>
					<td colspan="3">Total Unitário</td>
					<td>'.number_format($subtotal1,2,",",".").'</td>
					<td></td>
					</tr>
					<tr>
					<td colspan="3">Sub Total</td>
					<td>'.number_format($subtotal2,2,",",".").'</td>
					<td></td>
					</tr>';
					
					?>
					</tbody></table>
			</div>
			<div class="checkout-left">
				<a href="<?php echo base_url(); if($this->session->has_userdata('session_cliente')){ echo 'carrinho/finalizar';}else{ echo 'clientes/cadastro';}?>"><button class="submit check_out">Finalizar Pedido</button></a>
				<div class="clearfix"> </div>
			</div>

		</div>
<!-- //about -->
		</div>
		<div class="clearfix"></div>
	</div>
		<!--
  <script>
    $(function(){
      $('#example').okzoom({
        width: 150,
        height: 150,
        border: "1px solid black",
        shadow: "0 0 5px #000"
      });
    });
	
	$('.value-plus').on('click', function(){
		var divUpd = $(this).parent().find('.value'), newVal = parseInt(divUpd.text(), 10)+1;
		divUpd.text(newVal);
	});

	$('.value-minus').on('click', function(){
		var divUpd = $(this).parent().find('.value'), newVal = parseInt(divUpd.text(), 10)-1;
		if(newVal>=1) divUpd.text(newVal);
	});
	
	paypal.minicart.render({strings: {button: 'Finalizar'}, action: '<?php echo base_url('carrinho/checkout');?>'});
	var produtos = paypal.minicart.cart.items();
	var len = produtos.length;
	var prods = [];
	for (i = 0; i < len; i++) {
		prods.push(produtos[i].get());
	}
	
	$.ajax({
		type: "POST",
		url: "<?php echo base_url('carrinho/listar_produtos/');?>",
		data: {produtos: prods},
		success: function(data){
			$('#html_produtos').html(data);
			
			$('.value-plus').on('click', function(){
				var item = $(this).attr('id').split('_')[1];
				var divUpd = $(this).parent().find('.value'), newVal = parseInt(divUpd.text(), 10)+1;
				divUpd.text(newVal);
				alert(paypal.minicart.product.get(item));
			});

			$('.value-minus').on('click', function(){
				var item = $(this).attr('id').split('_')[1];
				var divUpd = $(this).parent().find('.value'), newVal = parseInt(divUpd.text(), 10)-1;
				if(newVal>=1) divUpd.text(newVal);
				alert(paypal.minicart.product.get(item));
			});
			
		}
	});
	</script>
quantity-->
<!--<div class="w3l_banner_nav_right">
		<br>
		<div class="w3l_banner_nav_right">
		<div class="agileinfo_single" style="padding:0;">
			<h5>Painel do Cliente</h5>
			
			<div class="clearfix"> </div>
		</div>
	</div>
	</div>
	<div class="clearfix"></div>
</div>-->
<?php $dadosCliente = $this->session->userdata('session_cliente');?>
<br>
<div class="col-lg-4">
	<h3>Meus Dados</h3>
	 <div class="form form-module" style="border-top:0; box-shadow:none; margin:0; margin-top:20px;">
		<form id="form_cad_cliente" action="#" method="post">
		  <input type="text" id="cadastro_username" name="Username" value="<?php echo $dadosCliente->nome;?>" placeholder="Nome" required=" ">
		  <input type="email" id="cadastro_email" name="Email" value="<?php echo $dadosCliente->email;?>" placeholder="E-mail" required=" ">
		  <input type="password" id="cadastro_password" name="Password" value="<?php echo $dadosCliente->senha;?>" placeholder="Senha" required=" ">
		  <input type="text" id="cadastro_telefone" name="Phone" value="<?php echo $dadosCliente->telefone;?>" placeholder="Telefone" required=" ">
		  <input type="submit" value="Salvar!">
		  <p id="retorno_cad_cliente"></p>
		</form>
	  </div>
</div>	

<div class="col-lg-5">
	<h3>Histórico de Pedidos</h3>
	<table class="table table-striped">
		<thead>
		  <tr>
			<th>Nº Ped.</th>
			<th>Data</th>
			<th>Status</th>
			<th>Total</th>
			<th></th>
		  </tr>
		</thead>
		<tbody>	
		  <?php 
		  $htmlDet = '';
		  foreach($pedidos as $pedido){ 
		  		  
			// Buscar itens do pedido
			$this->db->select('pedidos_det.qtd, pedidos_det.valor_unit, pedidos_det.valor_total, produtos.produto');
			$this->db->from('pedidos_det');
			$this->db->join('produtos', 'pedidos_det.id_produto = produtos.id');
			$this->db->where(array('pedidos_det.id_pedido' => $pedido->id));
			$query_pedidos_det = $this->db->get();
			$query_pedidos_det = $query_pedidos_det->result();
			$htmlDet .= '
			<div id="det_'.$pedido->id.'" class="modal fade" role="dialog">
			  <div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Detalhes do Pedido #'.$pedido->id.'</h4>
				  </div>
				  <div class="modal-body">
					<table class="table table-striped">
						<thead>
						  <tr>
							<th>Produto</th>
							<th>Quantidade</th>
							<th>Valor Unitário</th>
							<th>Valor Total</th>
						  </tr>
						</thead>
						<tbody>';
						foreach($query_pedidos_det as $det){
						  $htmlDet .= '<tr>
							<td>'.$det->produto.'</td>
							<td>'.$det->qtd.'</td>
							<td>'.number_format($det->valor_unit,2,",",".").'</td>
							<td>'.number_format($det->valor_total,2,",",".").'</td>
						  </tr>
						';
						}
						$htmlDet .= '</tbody>
					  </table>
				  </div>
				  <div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
				  </div>
				</div>
			  </div>
			</div>';
		  ?>		
		  <tr>
			<td><?php echo $pedido->id; ?></td>
			<td><?php echo date('d/m/Y H:i', strtotime($pedido->data_add)); ?></td>
			<td><?php echo $pedido->status_pedido; ?></td>
			<td><?php echo number_format($pedido->total_pedido,2,",","."); ?></td>
			<td><a href="#" data-toggle="modal" data-target="#det_<?php echo $pedido->id; ?>"><span style="color:gray;" class="glyphicon glyphicon-th-list" aria-hidden="true"></span></a> <a onClick="javascript:var msg = window.confirm('Deseja realmente cancelar o pedido #<?php echo $pedido->id; ?>?'); if(msg == true){ return true;}else{return false;}" href="<?php echo base_url('pedidos/cancelar/'.$pedido->id);?>"><span style="color:gray;" class="glyphicon glyphicon-remove" aria-hidden="true"></span></a></td>
		  </tr>		  
		  <?php } ?>
		</tbody>
	  </table>
</div>

<?php echo $htmlDet;?>

<script>
$('#form_cad_cliente').submit(function(){
	// $('#retorno_cad_cliente').html('Cadastrando usuário...');
	$.ajax({
		type: "POST",
		url: "<?php echo base_url('clientes/salvar/'.$dadosCliente->id);?>",
		data: {email: $('#cadastro_email').val(), senha: $('#cadastro_password').val(), nome: $('#cadastro_username').val(), telefone: $('#cadastro_telefone').val()},
		success: function(data){
			$('#retorno_cad_cliente').html(data);
			/*if(data.match('sucesso')){
				setTimeout(function(){
					window.location.href = "<?php echo base_url('clientes/painel');?>";
				}, 600);
			}*/
		}
	});
	return false;
});
</script>
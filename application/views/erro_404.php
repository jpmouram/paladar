﻿<body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <!-- page content -->
        <div class="col-md-12">
          <div class="col-middle">
            <div class="text-center text-center">
              <h1 class="error-number">404</h1>
              <h2>Pagina não exíste!</h2>
              <p>Essa página que você está tentando acessar não exíste <a href="#">Deseja reportar esse problema?</a>
              <p><a href="<?php echo base_url();?>">Clique aqui para retonar ao site.</a>
              </p>
            </div>
          </div>
        </div>
        <!-- /page content -->
      </div>
    </div>
  </html>
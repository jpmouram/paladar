<!DOCTYPE html>
<html lang="en">
<head>
  <title>Imobiliária Itacaré</title>
  <?php include "includes/headers.php"; ?>
</head>
<body>
<div class="container">
<div class="panel panel-default">
  <div class="panel-heading">Destaques</div>
  <div class="panel-body"><div class="row">
    <div class="col-sm-4">
      <div class="panel panel-primary">
        <div class="panel-heading">VENDA</div>
        <div class="panel-body"><!--<img src="http://s2.glbimg.com/Vvq-X0eJRT0_favHrTHTfndIzK4j8N0uD-FuSIdSoFJIoz-HdGixxa_8qOZvMp3w/e.glbimg.com/og/ed/f/original/2012/12/20/top_10_2012_praia_03.jpg" class="img-responsive" style="width:100%" alt="Image">-->
		
		<div id="myCarousel" class="carousel slide" data-ride="carousel">
		  <!-- Indicators -->
		  <ol class="carousel-indicators">
			<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			<li data-target="#myCarousel" data-slide-to="1"></li>
			<li data-target="#myCarousel" data-slide-to="2"></li>
		  </ol>

		  <!-- Wrapper for slides -->
		  <div class="carousel-inner">
			<div class="item active">
			  <img src="http://s2.glbimg.com/Vvq-X0eJRT0_favHrTHTfndIzK4j8N0uD-FuSIdSoFJIoz-HdGixxa_8qOZvMp3w/e.glbimg.com/og/ed/f/original/2012/12/20/top_10_2012_praia_03.jpg" alt="Los Angeles">
			</div>

			<div class="item">
			  <img src="http://www.elhombre.com.br/wp-content/uploads/2013/03/Casa-na-praia.jpg" alt="Chicago">
			</div>

			<div class="item">
			  <img src="http://images.arquidicas.com.br/wp-content/uploads/2014/05/12192448/casa-praia-conatiner.jpg" alt="New York">
			</div>
		  </div>

		  <!-- Left and right controls -->
		  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left"></span>
			<span class="sr-only">Previous</span>
		  </a>
		  <a class="right carousel-control" href="#myCarousel" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right"></span>
			<span class="sr-only">Next</span>
		  </a>
		</div>
		
		</div>
        <div class="panel-footer">Quartos <span style="float:right;">3</span></div>
		<div class="panel-footer">Banheiros <span style="float:right;">3</span></div>
		<div class="panel-footer">Garagem <span style="float:right;">2 Carros</span></div>
		<div class="panel-footer">Valor <span style="float:right;">R$ 450.000,00</span></div>
      </div>
    </div>
    <div class="col-sm-4"> 
      <div class="panel panel-danger">
        <div class="panel-heading">ALUGUEL</div>
        <div class="panel-body"><img src="http://www.elhombre.com.br/wp-content/uploads/2013/03/Casa-na-praia.jpg" class="img-responsive" style="width:100%" alt="Image"></div>
        <div class="panel-footer">Quartos <span style="float:right;">3</span></div>
		<div class="panel-footer">Banheiros <span style="float:right;">3</span></div>
		<div class="panel-footer">Garagem <span style="float:right;">2 Carros</span></div>
		<div class="panel-footer">Valor <span style="float:right;">R$ 450.000,00</span></div>
      </div>
    </div>
    <div class="col-sm-4"> 
      <div class="panel panel-success">
        <div class="panel-heading">ARRENDAMENTO</div>
        <div class="panel-body"><img src="http://images.arquidicas.com.br/wp-content/uploads/2014/05/12192448/casa-praia-conatiner.jpg" class="img-responsive" style="width:100%" alt="Image"></div>
        <div class="panel-footer">Quartos <span style="float:right;">3</span></div>
		<div class="panel-footer">Banheiros <span style="float:right;">3</span></div>
		<div class="panel-footer">Garagem <span style="float:right;">2 Carros</span></div>
		<div class="panel-footer">Valor <span style="float:right;">R$ 450.000,00</span></div>
      </div>
    </div>
  </div>
  </div>
</div>
</div>
<br>

<div class="container">  
<?php for($a=1; $a<=2; $a++){?>
  <div class="row">
  <?php for($b=1; $b<=3; $b++){?>
    <div class="col-sm-4">
      <div class="panel panel-primary">
        <div class="panel-heading"><a href="<?php echo base_url('/home/imovel/1/nome-do-imovel');?>" style="color:#FFF;">VENDA</a></div>
        <div class="panel-body">
		
		<!-- Images -->
		<div id="sem-destaque-carrosel-<?php echo $a;?>-<?php echo $b;?>" class="carousel slide" data-ride="carousel">
		  <!-- Indicators -->
		  <ol class="carousel-indicators">
			<li data-target="#sem-destaque-carrosel-<?php echo $a;?>-<?php echo $b;?>" data-slide-to="0" class="active"></li>
			<li data-target="#sem-destaque-carrosel-<?php echo $a;?>-<?php echo $b;?>" data-slide-to="1"></li>
			<li data-target="#sem-destaque-carrosel-<?php echo $a;?>-<?php echo $b;?>" data-slide-to="2"></li>
		  </ol>

		  <!-- Wrapper for slides -->
		  <div class="carousel-inner">
			<div class="item active">
			  <img src="http://s2.glbimg.com/Vvq-X0eJRT0_favHrTHTfndIzK4j8N0uD-FuSIdSoFJIoz-HdGixxa_8qOZvMp3w/e.glbimg.com/og/ed/f/original/2012/12/20/top_10_2012_praia_03.jpg" alt="Los Angeles">
			</div>

			<div class="item">
			  <img src="http://www.elhombre.com.br/wp-content/uploads/2013/03/Casa-na-praia.jpg" alt="Chicago">
			</div>

			<div class="item">
			  <img src="http://images.arquidicas.com.br/wp-content/uploads/2014/05/12192448/casa-praia-conatiner.jpg" alt="New York">
			</div>
		  </div>

		  <!-- Left and right controls -->
		  <a class="left carousel-control" href="#sem-destaque-carrosel-<?php echo $a;?>-<?php echo $b;?>" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left"></span>
			<span class="sr-only">Previous</span>
		  </a>
		  <a class="right carousel-control" href="#sem-destaque-carrosel-<?php echo $a;?>-<?php echo $b;?>" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right"></span>
			<span class="sr-only">Next</span>
		  </a>
		</div>
		
		</div>
        <div class="panel-footer">Quartos <span style="float:right;">3</span></div>
		<div class="panel-footer">Banheiros <span style="float:right;">3</span></div>
		<div class="panel-footer">Garagem <span style="float:right;">2 Carros</span></div>
		<div class="panel-footer">Valor <span style="float:right;">R$ 450.000,00</span></div>
      </div>
    </div>
	<?php } ?>
  </div>
<?php } ?>
</div><br><br>


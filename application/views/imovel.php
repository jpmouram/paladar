<div class="container">
<!--
<div class="topoImovel">
	<div class="col-sm-7">
		<h1>Casa com 5 Quartos à Venda, 550 m² por R$ 2.200.000</h1>
		<h2>Ilha do João Araújo, Parati - RJ</h2>
	</div>
	<div class="col-sm-5">
		<h1 class="text-right">Carlos Alberto de Nóbrega <i class="glyphicon glyphicon-user"></i></h1>
		<h2 class="text-right">(11) 72167-12342 <i class="glyphicon glyphicon-earphone"></i></h2>
	</div>
</div>
-->
<div class="col-sm-12">
<?php for($a=1; $a<=1; $a++){?>
  <div class="row">
  <?php for($b=1; $b<=1; $b++){?>
    <div class="col-sm-7">
		<!-- Images -->
		<div id="sem-destaque-carrosel-<?php echo $a;?>-<?php echo $b;?>" class="carousel slide" data-ride="carousel">
		  <!-- Indicators -->
		  <ol class="carousel-indicators">
			<li data-target="#sem-destaque-carrosel-<?php echo $a;?>-<?php echo $b;?>" data-slide-to="0" class="active"></li>
			<li data-target="#sem-destaque-carrosel-<?php echo $a;?>-<?php echo $b;?>" data-slide-to="1"></li>
			<li data-target="#sem-destaque-carrosel-<?php echo $a;?>-<?php echo $b;?>" data-slide-to="2"></li>
			<li data-target="#sem-destaque-carrosel-<?php echo $a;?>-<?php echo $b;?>" data-slide-to="3"></li>
		  </ol>

		  <!-- Wrapper for slides -->
		  <div class="carousel-inner">
			<div class="item active">
			  <img src="https://garfosequartos.files.wordpress.com/2012/08/dsc07676.jpg" alt="Los Angeles">
			</div>

			<div class="item">
			  <img src="http://cdn1.valuegaia.com.br/_Fotos/2055/2221/2055C1845C8D4ACCA1ACCA5CCA557253EA6282B327B997540.JPG" alt="Chicago">
			</div>

			<div class="item">
			  <img src="http://www.iglesiasimoveis.com.br/imagens/SAM_8060.JPG" alt="New York">
			</div>
			
			<div class="item">
			  <img src="http://cdn1.imoveisbrasilbahia.com.br/uploads/properties/316869b191.jpg" alt="New York">
			</div>
		  </div>

		  <!-- Left and right controls -->
		  <a class="left carousel-control" href="#sem-destaque-carrosel-<?php echo $a;?>-<?php echo $b;?>" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left"></span>
			<span class="sr-only">Previous</span>
		  </a>
		  <a class="right carousel-control" href="#sem-destaque-carrosel-<?php echo $a;?>-<?php echo $b;?>" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right"></span>
			<span class="sr-only">Next</span>
		  </a>
		</div>
		</div>
		<div class="col-sm-5">
			<div class="panel-footer">Area m² <span style="float:right;">450 m²</span></div>
			<div class="panel-footer">Quartos <span style="float:right;">3</span></div>
			<div class="panel-footer">Banheiros <span style="float:right;">3</span></div>
			<div class="panel-footer">Garagem <span style="float:right;">2 Carros</span></div>
			<div class="panel-footer">Valor <span style="float:right;">R$ 450.000,00</span></div>
			<div class="panel-footer">Condomínio <span style="float:right;">R$ 540,00</span></div>
			<br>
			<form>
			  <div class="input-group">
				<span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
				<input id="nome" type="text" class="form-control" name="nome" placeholder="Nome">
			  </div><br>
			  <div class="input-group">
				<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
				<input id="email" type="text" class="form-control" name="email" placeholder="Email">
			  </div><br>
			  <div class="input-group">
				<span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
				<input id="telefone" type="telefone" class="form-control" name="text" placeholder="Telefone">
			  </div><br>
			  <div class="input-group">
				<span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
				<input id="msg" type="text" class="form-control" name="msg" placeholder="Mensagem">
			  </div><br>
			  <button type="button" class="btn" style="float:right;">Enviar Contato</button>
			</form>
			
		</div>
	<?php } ?>
  </div>
<?php } ?>
</div>
</div><br><br>

<!-- Descrição do Imóvel -->
<div class="container">
<div class="panel panel-default">
  <div class="panel-heading">Descrição do Imóvel</div>
  <div class="panel-body">BELÍSSIMA CASA MOBILIADA NA ILHA DO ARAUJO COM 550 M² DE ÁREA CONSTRUÍDA EM TERRENO DE 2.500 M². A PROPRIEDADE POSSUI ÁGUA DOCE, ENERGIA ELÉTRICA , PIER COM CALADO MÉDIO , 04 QUARTOS SENDO 03 SUITES, SALA DE ESTAR, SALA DE JANTAR,SALA DE TV, MEZANINO, COZINHA, ÁREA DE SERVIÇO. NA ÁREA EXTERNA DO IMÓVEL, ALÉM DE UM AMPLO QUINTAL COM JARDIM, ENCONTRA-SE 01 DORMITÓRIO, PISCINA COM VISTA PARA O MAR E A SERRA, BAR, CHURRASQUEIRA, FORNO A LENHA, OFURÔ DENTRO DA PISCINA, SAUNA A VAPOR ACOPLADA A PISCINA, DUCHA E BANHEIRO.</div>
</div>
</div>

<!-- Características do Imóvel -->
<div class="container">
<div class="panel panel-default">
  <div class="panel-heading">Características do Imóvel</div>
  <div class="panel-body">
	<ul class="col-sm-4">
		<li>Espaço Gourmet</li>
		<li>Cais para 2 barcos</li>
		<li>Piscina</li>
	</ul>
	
	<ul class="col-sm-4">
		<li>Espaço Gourmet</li>
		<li>Cais para 2 barcos</li>
		<li>Piscina</li>
	</ul>
	
	<ul class="col-sm-4">
		<li>Espaço Gourmet</li>
		<li>Cais para 2 barcos</li>
		<li>Piscina</li>
	</ul>
  </div>
</div>
</div>


<!-- Localização do Imóvel -->
<div class="container">
<div class="panel panel-default">
  <div class="panel-heading">Localização do Imóvel</div>
  <div class="panel-body"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d295.71763758635404!2d-39.00228054288928!3d-14.345032516196174!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x25547c7e3739ea30!2sPraia+de+Jeribuca%C3%A7u!5e0!3m2!1spt-BR!2sbr!4v1504667409462" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></div>
</div>
</div>
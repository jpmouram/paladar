
<!-- newsletter 
<div class="newsletter">
	<div class="container">
		<div class="w3agile_newsletter_left">
			<h3>Receba nossas novidades</h3>
		</div>
		<div class="w3agile_newsletter_right">
			<form action="#" method="post">
				<input type="email" name="Email" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
				<input type="submit" value="assinar agora">
			</form>
		</div>
		<div class="clearfix"> </div>
	</div>
</div>-->

<div class="footer background-azul-1">
	<div class="container">
		<!--
		<div class="col-md-3 w3_footer_grid">
			<h3>informações</h3>
			<ul class="w3_footer_grid_list">
				<li><a href="events.html">Events</a></li>
				<li><a href="about.html">About Us</a></li>
				<li><a href="products.html">Best Deals</a></li>
				<li><a href="services.html">Services</a></li>
				<li><a href="short-codes.html">Short Codes</a></li>
			</ul>
		</div>
		<div class="col-md-3 w3_footer_grid">
			<h3>Política da Empresa</h3>
			<ul class="w3_footer_grid_list">
				<li><a href="faqs.html">FAQ</a></li>
				<li><a href="privacy.html">privacy policy</a></li>
				<li><a href="privacy.html">terms of use</a></li>
			</ul>
		</div>
		<div class="col-md-3 w3_footer_grid">
			<h3>O que fazemos</h3>
			<ul class="w3_footer_grid_list">
				<li><a href="pet.html">Pet Food</a></li>
				<li><a href="frozen.html">Frozen Snacks</a></li>
				<li><a href="kitchen.html">Kitchen</a></li>
				<li><a href="products.html">Branded Foods</a></li>
				<li><a href="household.html">Households</a></li>
			</ul>
		</div>
		-->
		<div class="col-md-4 w3_footer_grid">
			<h3>contatos</h3>
			<ul class="w3_footer_grid_list">
				<li><a href="#">contato@paladar.com.br</a></li>
				<li><a href="#">Alameda Francisco de Souza Braga, 1021</a></li>
				<li><a href="#">Jardim California - 14706-230</a></li>
				<li><a href="#">Bebedouro - SP</a></li>
			</ul>
		</div>
		<div class="col-md-4 w3_footer_grid">
			<h3>como chegar</h3>
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3726.320772099185!2d-48.506461184982236!3d-20.939629586046788!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94bbe46ead47f17f%3A0xfb5c838b2c7e0e3e!2sAlameda+Francisco+de+Souza+Braga%2C+1021+-+Jardim+California%2C+Bebedouro+-+SP%2C+14706-230!5e0!3m2!1spt-BR!2sbr!4v1513648440784" width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
		<div class="col-md-4 w3_footer_grid">
			<h3>conecte-se a nós</h3>
			<ul class="agileits_social_icons">
				<li><a href="#" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
				<li><a href="#" class="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
				<li><a href="#" class="google"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
				<li><a href="#" class="instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
				<li><a href="#" class="dribbble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
			</ul>
		</div>		
	</div>
</div>

<!-- Modal Carrinho -->
<div id="modalCarrinho" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Carrinho</h4>
      </div>
      <div class="modal-body" id="produtos_carrinho">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>

  </div>
</div>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url('assets_site/');?>js/bootstrap.min.js"></script>
<script>
$(document).ready(function(){
    $(".dropdown").hover(            
        function() {
            $('.dropdown-menu', this).stop( true, true ).slideDown("fast");
            $(this).toggleClass('open');        
        },
        function() {
            $('.dropdown-menu', this).stop( true, true ).slideUp("fast");
            $(this).toggleClass('open');       
        }
    );
});
</script>
<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			// $().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
<!-- //here ends scrolling icon
<script src="<?php echo base_url('assets_site/');?>js/minicart.js"></script>
<script>
		paypal.minicart.render({strings: {button: 'Finalizar'}, action: '<?php echo base_url('carrinho/checkout');?>'});

		paypal.minicart.cart.on('checkout', function (evt) {
			var items = this.items(),
				len = items.length,
				total = 0,
				i;

			// Count the number of each item in the cart
			for (i = 0; i < len; i++) {
				total += items[i].get('quantity');
			}
			/*
			if (total < 3) {
				alert('The minimum order quantity is 3. Please add more to your shopping cart before checking out');
				evt.preventDefault();
			}
			*/
		});
		-->
	<script>
	$(function(){
		$('.form_produtos').submit(function(){
			var form_id = $(this).attr('id');
			var form = '#'+form_id;
			var produto_nome = $(form+' input[name=item_name]').val();
			var produto_id = $(form+' input[name=add]').val();
			var produto_valor = ($(form+' input[name=amount]').val() - $(form+' input[name=discount_amount]').val());
			$.ajax({
				type: "POST",
				url: "<?php echo base_url('carrinho/add');?>",
				data: {produto_id: produto_id, produto_nome: produto_nome, produto_valor: produto_valor},
				success: function(data){
					// $('#produtos_carrinho').html(data);
					// $('#modalCarrinho').modal();
					window.location.href = "<?php echo base_url('carrinho/checkout');?>";
				}
			});
			return false;
		});
	})
	</script>
	 
</body>
</html>
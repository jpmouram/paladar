<!DOCTYPE html>
<html>
<head>

<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Grocery Store Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="<?php echo base_url('assets_site/');?>css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url('assets_site/');?>css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- font-awesome icons -->
<link href="<?php echo base_url('assets_site/');?>css/font-awesome.css" rel="stylesheet" type="text/css" media="all" /> 
<!-- //font-awesome icons -->
<!-- js -->
<script src="<?php echo base_url('assets_site/');?>js/jquery-1.11.1.min.js"></script>
<!-- //js -->
<link href='//fonts.googleapis.com/css?family=Ubuntu:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="<?php echo base_url('assets_site/');?>js/move-top.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets_site/');?>js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smoth-scrolling -->
<style>
.logo_products{padding:10px; background:#FFFFFF; border-bottom:3px solid #29166f;}
.banner_bottom, .top-brands, .fresh-vegetables, .w3l_banner_nav_right_banner3_btm, .testimonials, .team, .newsletter-top-serv-btm, .w3ls_w3l_banner_nav_right_grid_sub{
    padding-top: 0 !important;
}
.newsletter{clear:both; margin-top:10px;}
.w3ls_w3l_banner_nav_right_grid1{margin-bottom:10px;}
.navbar-nav > li > a{border:0; padding:15px;}
/*.navbar-nav > li{background:#e64c3c;}*/
.form-module .toggle{padding:5px; width:auto; height:auto;}
td.invert-image a img{width:70px;}
.privacy{padding: 1em;}
.w3ls_logo_products_left h1 a{background:none; height:auto;}
.products-breadcrumb, .agileits_header{background:#29166f;}
.product_list_header{position:relative; top:5px;}
.color-vermelho-1{color:#db241b;}
.color-azul-1{color:#29166f !important;}
.background-azul-1{background:#29166f !important;}
.background-vermelho-1{background:#db241b !important;}
.products-breadcrumb{display:none;}
.margin-top{margin-top:50px; margin-bottom:60px;}
</style>

<title>Distribuidora Paladar Friopan</title>
</head>
	
<body>
<!-- header -->
	<?php $mostrarIsso = '2S'; if($mostrarIsso == 'S'){?>
	<div class="agileits_header">
		<!--
		<div class="w3l_offers">
			<a href="<?php echo base_url();?>" class="background-vermelho-1">Paladar Friopan</a>
		</div>-->
		<div class="w3l_search">
			<form action="#" method="post">
				<input type="text" name="Product" value="Procurar um produto..." onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search a product...';}" required="">
				<input type="submit" value=" ">
			</form>
		</div>
		
		<?php if($this->session->has_userdata('carrinho')){ ?>
		<div class="product_list_header">
			<a title="Meu Carrinho" href="<?php echo base_url('carrinho/checkout');?>"><img src="<?php echo base_url('assets_site/images/shopping-cart-button.png');?>" width="35"></a>
		</div>
		<?php } ?>
		
		<div class="w3l_header_right">
			<ul>
				<li class="dropdown profile_details_drop">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user" aria-hidden="true"></i><span class="caret"></span></a>
					<div class="mega-dropdown-menu">
						<div class="w3ls_vegetables">
							<ul class="dropdown-menu drp-mnu">
								<?php if(!$this->session->userdata('session_cliente')){ ?>
								<li><a href="<?php echo base_url('clientes/cadastro');?>">Entrar</a></li> 
								<li><a href="<?php echo base_url('clientes/cadastro');?>">Cadastre-se</a></li>
								<?php }else{ ?>
								<li><a href="<?php echo base_url('clientes/painel');?>">Meus Dados</a></li>
								<li><a href="<?php echo base_url('clientes/painel');?>">Meus Pedidos</a></li>
								<li><a href="<?php echo base_url('clientes/sair');?>">Sair</a></li>
								<?php } ?>
							</ul>
						</div>                  
					</div>	
				</li>
			</ul>
		</div>
		<div class="w3l_header_right1">
			<h2><a href="mail.html">Contate-nos</a></h2>
		</div>
		<div class="clearfix"> </div>
	</div>
	<br><br>
	<?php } ?>
<!-- //script-for sticky-nav -->
	<div class="logo_products fixed">
		<div class="container">
			<div class="w3ls_logo_products_left">
				<h1><a href="<?php echo base_url();?>" class="color-azul-1"><span class="color-vermelho-1">Paladar</span> Friopan</a></h1>
			</div>
			<div class="w3ls_logo_products_left1">
				<ul class="special_items">
					<li><a href="events.html">Contato</a><i>/</i></li>
					<li><a href="about.html">Sobre nós</a><i>/</i></li>
					<li><a href="products.html">Produtos</a><i>/</i>
					
					</li>
					<li><a href="services.html">Serviços</a></li>
				</ul>
			</div>
			<div class="w3ls_logo_products_left1">
				<ul class="phone_email">
					<li><i class="fa fa-phone" aria-hidden="true"></i>(17) 3342-4447</li>
					<!--<li><i class="fa fa-envelope-o" aria-hidden="true"></i><a href="mailto:contato@paladar.com.br">contato@paladar.com.br</a></li>-->
				</ul>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
	
	
	<div class="clearfix margin-top"> </div>
	<!-- 
	<nav class="navbar navbar-inverse">
	  <div class="container-fluid">
		<div class="navbar-header">
		  <a class="navbar-brand" href="#">WebSiteName</a>
		</div>
		<ul class="nav navbar-nav">
		  <li class="active"><a href="#">Home</a></li>
		  <li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#">Page 1
			<span class="caret"></span></a>
			<ul class="dropdown-menu">
			  <li><a href="#">Page 1-1</a></li>
			  <li><a href="#">Page 1-2</a></li>
			  <li><a href="#">Page 1-3</a></li>
			</ul>
		  </li>
		  <li><a href="#">Page 2</a></li>
		  <li><a href="#">Page 3</a></li>
		</ul>
	  </div>
	</nav>
	
//header 

<div class="products-breadcrumb">
		<div class="container">
			<ul>
				<li><i class="fa fa-home" aria-hidden="true"></i><a href="<?php echo base_url();?>">Home</a></li>
				<li><span>|</span><?php echo ucwords($this->uri->segment(1));?></li>
				<li><span>|</span><?php echo ucwords($this->uri->segment(2));?></li>
				<li><span>|</span><?php echo ucwords($this->uri->segment(3));?></li>
				<li><span>|</span><?php echo ucwords(urldecode($this->uri->segment(4)));?></li>
			</ul>
		</div>
	</div>
-->
<div class="products-breadcrumb"></div>	
<!-- banner -->
<div class="banner">
	<div class="w3l_banner_nav_left">
		<nav class="navbar nav_bottom">
		 <!-- Brand and toggle get grouped for better mobile display -->
		  <div class="navbar-header nav_2">
			  <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
		   </div> 
		   
		   <!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
				<ul class="nav navbar-nav nav_1">
				<?php 
				#for($i=1; $i<=3; $i++){
				foreach($categorias as $row){ $ids = explode('||', $row->ids); $subs = explode('||', $row->subs);?>
					<li class="dropdown mega-dropdown active">
						<a href="<?php echo base_url('produtos/listar/'.$row->categoria.'/');?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $row->categoria ?><span class="caret"></span></a>				
						<div class="dropdown-menu mega-dropdown-menu w3ls_vegetables_menu">
							<div class="w3ls_vegetables">
								<ul>	
									<?php $c=0; foreach($ids as $id_sub){ ?>
										<li><a href="<?php echo base_url('produtos/listar/'.$row->categoria.'/'.$subs[$c]);?>"><?php echo $subs[$c];?></a></li>
									<?php $c++; } ?>
								</ul>
							</div>                  
						</div>				
					</li>
					<?php } ?>
					<?php #} ?>
				</ul>
			 </div><!-- /.navbar-collapse -->
		</nav>
	</div>
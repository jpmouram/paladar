<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">Imobiliária Itacaré</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="./">Home</a></li>
        <li><a href="#">Comprar</a></li>
		<li><a href="#">Alugar</a></li>
		<li><a href="#">Temporada</a></li>
		<li><a href="#">Arrendar</a></li>
		<li><a href="#">Vender</a></li>
        <li><a href="#">Conheça Itacaré</a></li>
        <li><a href="<?php echo base_url('fale-conosco');?>">Contato</a></li>
      </ul>
      <!--<ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-user"></span> Your Account</a></li>
        <li><a href="#"><span class="glyphicon glyphicon-shopping-cart"></span> Cart</a></li>
      </ul>-->
    </div>
  </div>
</nav>
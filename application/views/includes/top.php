<!DOCTYPE html>
<html lang="en">
<head>
<title><?php echo $titleWebsite;?></title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css');?>">
<script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
<script src="<?php echo base_url('assets/js/jquery.maskMoney.js');?>"></script>
<script src="<?php echo base_url('assets/js/jquery.mask.js');?>"></script>
<script>
$(function(){
  $('.carousel').carousel('pause');
})
</script>
<style>
/* Remove the navbar's default rounded borders and increase the bottom margin */ 
.navbar {
  margin-bottom: 50px;
  border-radius: 0;
}

/* Remove the jumbotron's default bottom margin */ 
 .jumbotron {
  margin-bottom: 0;
  color:#cecece;
}


/* Add a gray background color and some padding to the footer */
footer {
  background-color: #f2f2f2;
  padding: 25px;
}

.loader {
    border: 4px solid #f3f3f3; /* Light grey */
    border-top: 4px solid #3498db; /* Blue */
    border-radius: 50%;
    width: 25px;
    height: 25px;
    animation: spin 2s linear infinite;
}

@keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
}


</style>
</head>
<body>

<div class="jumbotron" style="background-image:url('http://matuete.com/wpmatuete/wp-content/uploads/2013/12/102_2279-site.jpg');background-repeat: no-repeat;
    background-position: center;
    background-size: cover;
    width: 100%;
    height: 100%;">
  <div class="container text-center">
	<?php if($titleWebsite <> ''){ ?>
    <h1><?php echo $titleWebsite;?></h1>      
	<?php }
	if($descSite <> ''){
	?>
    <p>Aluguel, Temporada, Venda & Arrendamentos</p>
	<?php } ?>
  </div>
</div>
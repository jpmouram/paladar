	<div class="w3l_banner_nav_right">
<!-- login -->
		<br>
		<div class="w3_login" style="padding:0;">
			<h3>Log in & Cadastro</h3>
			<div class="w3_login_module">
				<div class="module form-module">
				  <div class="toggle">Cadastre-se
					<!--<div class="tooltip">Cadastrar</div>-->
				  </div>
				  <div class="form">
					<h2>Entrar</h2>
					<form id="form_login_cliente" action="#" method="post">
					  <input type="email" id="login_username" name="Username" placeholder="E-mail" required=" ">
					  <input type="password" id="login_password" name="Password" placeholder="Senha" required=" ">
					  <input type="submit" value="Entrar!">
					  <p id="retorno_login_cliente"></p>
					</form>
				  </div>
				  <div class="form">
					<h2>Criar uma conta</h2>
					<form id="form_cad_cliente" action="#" method="post">
					  <input type="text" id="cadastro_username" name="Username" placeholder="Nome" required=" ">
					  <input type="email" id="cadastro_email" name="Email" placeholder="E-mail" required=" ">
					  <input type="password" id="cadastro_password" name="Password" placeholder="Senha" required=" ">
					  <input type="text" id="cadastro_telefone" name="Phone" placeholder="Telefone" required=" ">
					  <input type="submit" value="Cadastrar!">
					  <p id="retorno_cad_cliente"></p>
					</form>
				  </div>
				  <div class="cta"><a href="#">Esqueceu sua senha?</a></div>
				</div>
			</div>
			<script>
			
				$('#form_cad_cliente').submit(function(){
					// $('#retorno_cad_cliente').html('Cadastrando usuário...');
					$.ajax({
						type: "POST",
						url: "<?php echo base_url('clientes/salvar');?>",
						data: {email: $('#cadastro_email').val(), senha: $('#cadastro_password').val(), nome: $('#cadastro_username').val(), telefone: $('#cadastro_telefone').val()},
						success: function(data){
							$('#retorno_cad_cliente').html(data);
							if(data.match('sucesso')){
								setTimeout(function(){
									// window.location.href = "<?php echo base_url('clientes/painel');?>";
								}, 600);
							}
						}
					});
					return false;
				});
				
				$('#form_login_cliente').submit(function(){
					// $('#retorno_login_cliente').html('Verificando usuário...');
					$.ajax({
						type: "POST",
						url: "<?php echo base_url('clientes/login');?>",
						data: {email: $('#login_username').val(), senha: $('#login_password').val()},
						success: function(data){
							$('#retorno_login_cliente').html(data);
							if(data.match('sucesso')){
								setTimeout(function(){
									window.location.href = "<?php if($this->session->has_userdata('carrinho')){
										echo base_url('carrinho/checkout');
									}else{
										echo base_url('clientes/painel');
									}?>";
								}, 600);
							}
						}
					});
					return false;
				});
			
				$('.toggle').click(function(){
				  // Switches the Icon
				  $(this).children('i').toggleClass('fa-pencil');
				  // Switches the forms  
				  $('.form').animate({
					height: "toggle",
					'padding-top': 'toggle',
					'padding-bottom': 'toggle',
					opacity: "toggle"
				  }, "slow");
				});
			</script>
		</div>
<!-- //login -->
		</div>
		<div class="clearfix"></div>
	</div>
	
	<script src='<?php echo base_url('assets_site/js/jquery-1.11.1.min.js');?>'></script>
	<script src='<?php echo base_url('assets_site/js/okzoom.js');?>'></script>
  <script>
    $(function(){
      $('#example').okzoom({
        width: 150,
        height: 150,
        border: "1px solid black",
        shadow: "0 0 5px #000"
      });
    });
  </script>
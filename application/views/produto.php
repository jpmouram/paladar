<?php

$produto = $produtos[0];

 $mostrarIsso = 'N'; if($mostrarIsso == 'S'){?>
		<div class="w3l_banner_nav_right">
			<section class="slider">
				<div class="flexslider">
					<ul class="slides">
						<li>
							<div class="w3l_banner_nav_right_banner">
								<h3>Make your <span>food</span> with Spicy.</h3>
								<div class="more">
									<a href="products.html" class="button--saqui button--round-l button--text-thick" data-text="Shop now">Shop now</a>
								</div>
							</div>
						</li>
						<li>
							<div class="w3l_banner_nav_right_banner1">
								<h3>Make your <span>food</span> with Spicy.</h3>
								<div class="more">
									<a href="products.html" class="button--saqui button--round-l button--text-thick" data-text="Shop now">Shop now</a>
								</div>
							</div>
						</li>
						<li>
							<div class="w3l_banner_nav_right_banner2">
								<h3>upto <i>50%</i> off.</h3>
								<div class="more">
									<a href="products.html" class="button--saqui button--round-l button--text-thick" data-text="Shop now">Shop now</a>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</section>
			<!-- flexSlider -->
				<link rel="stylesheet" href="<?php echo base_url('assets_site/');?>css/flexslider.css" type="text/css" media="screen" property="" />
				<script defer src="<?php echo base_url('assets_site/');?>js/jquery.flexslider.js"></script>
				<script type="text/javascript">
				$(window).load(function(){
				  $('.flexslider').flexslider({
					animation: "slide",
					start: function(slider){
					  $('body').removeClass('loading');
					}
				  });
				});
			  </script>
			<!-- //flexSlider -->
		</div>
		<div class="clearfix"></div>
		<?php } ?>
	</div>
	
	<div class="w3l_banner_nav_right">
			<?php /*
			<div class="w3l_banner_nav_right_banner5">
				<h3><?php if($this->uri->segment(4)){echo urldecode($this->uri->segment(4));}?></h3>
			</div>
			*/?>
			<br>
			<div class="w3l_banner_nav_right">
			<div class="agileinfo_single" style="padding:0;">
				<h5><?php echo $produtos[0]->produto;?></h5>
				<div class="col-md-4 agileinfo_single_left">
					<img id="example" src="<?php echo base_url('imagens/produtos/'.$produtos[0]->imagem);?>" alt=" " class="img-responsive" />
				</div>
				<div class="col-md-8 agileinfo_single_right">
					<div class="w3agile_description">
						<h4>Descrição :</h4>
						<p><?php echo $produtos[0]->descricao;?></p>
					</div>
					<div class="snipcart-item block">
						<div class="snipcart-thumb agileinfo_single_right_snipcart">
							<h4>R$ <?php echo $produtos[0]->valor_promocao;?> <span>R$ <?php echo $produtos[0]->valor;?></span></h4>
						</div>
						<div class="snipcart-details agileinfo_single_right_details">
							<form action="#" method="post" class="form_produtos" id="form_produto_<?php echo $produto->id; ?>">
								<fieldset>
									<input type="hidden" name="cmd" value="_cart" />
									<input type="hidden" name="add" value="<?php echo $produto->id; ?>" />
									<input type="hidden" name="business" value="<?php echo $produto->id; ?>" />
									<input type="hidden" name="item_name" value="<?php echo $produto->produto; ?>" />
									<input type="hidden" name="amount" value="<?php echo $produto->valor; ?>" />
									<input type="hidden" name="discount_amount" value="<?php echo ($produto->valor - $produto->valor_promocao); ?>" />
									<input type="hidden" name="currency_code" value="BRL" />
									<input type="hidden" name="return" value=" " />
									<input type="hidden" name="cancel_return" value=" " />
									<input type="submit" name="submit" value="Adicionar ao Carrinho" class="button" />
								</fieldset>
							</form>
						</div>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
		</div>
		<div class="clearfix"></div>
	</div>
	
	<script src='<?php echo base_url('assets_site/js/jquery-1.11.1.min.js');?>'></script>
	<script src='<?php echo base_url('assets_site/js/okzoom.js');?>'></script>
  <script>
    $(function(){
      $('#example').okzoom({
        width: 150,
        height: 150,
        border: "1px solid black",
        shadow: "0 0 5px #000"
      });
    });
  </script>
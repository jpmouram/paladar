<?php $mostrarIsso = 'N'; if($mostrarIsso == 'S'){?>
		<div class="w3l_banner_nav_right">
			<section class="slider">
				<div class="flexslider">
					<ul class="slides">
						<li>
							<div class="w3l_banner_nav_right_banner">
								<h3>Make your <span>food</span> with Spicy.</h3>
								<div class="more">
									<a href="products.html" class="button--saqui button--round-l button--text-thick" data-text="Shop now">Shop now</a>
								</div>
							</div>
						</li>
						<li>
							<div class="w3l_banner_nav_right_banner1">
								<h3>Make your <span>food</span> with Spicy.</h3>
								<div class="more">
									<a href="products.html" class="button--saqui button--round-l button--text-thick" data-text="Shop now">Shop now</a>
								</div>
							</div>
						</li>
						<li>
							<div class="w3l_banner_nav_right_banner2">
								<h3>upto <i>50%</i> off.</h3>
								<div class="more">
									<a href="products.html" class="button--saqui button--round-l button--text-thick" data-text="Shop now">Shop now</a>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</section>
			<!-- flexSlider -->
				<link rel="stylesheet" href="<?php echo base_url('assets_site/');?>css/flexslider.css" type="text/css" media="screen" property="" />
				<script defer src="<?php echo base_url('assets_site/');?>js/jquery.flexslider.js"></script>
				<script type="text/javascript">
				$(window).load(function(){
				  $('.flexslider').flexslider({
					animation: "slide",
					start: function(slider){
					  $('body').removeClass('loading');
					}
				  });
				});
			  </script>
			<!-- //flexSlider -->
		</div>
		<div class="clearfix"></div>
		<?php } ?>
	</div>
	
	<div class="w3l_banner_nav_right col-sm-6">
			<?php /*
			<div class="w3l_banner_nav_right_banner5">
				<h3><?php echo urldecode($this->uri->segment(3));?> <?php if($this->uri->segment(4)){echo ' - '.urldecode($this->uri->segment(4));}?></h3>
			</div>
			*/ ?>
			<br>
			<div class="w3ls_w3l_banner_nav_right_grid w3ls_w3l_banner_nav_right_grid_veg">
				<h3 class="w3l_fruit"><?php echo urldecode($this->uri->segment(3));?> <?php if($this->uri->segment(4)){echo ' - '.urldecode($this->uri->segment(4));}?></h3>
				<div class="w3ls_w3l_banner_nav_right_grid1 w3ls_w3l_banner_nav_right_grid1_veg">
					<?php foreach($produtos as $produto){ ?>
					<div class="col-md-3 w3ls_w3l_banner_left w3ls_w3l_banner_left_asdfdfd">
						<div class="hover14 column">
						<div class="agile_top_brand_left_grid w3l_agile_top_brand_left_grid">
							<div class="tag"><img src="images/tag.png" alt=" " class="img-responsive"></div>
							<div class="agile_top_brand_left_grid1">
								<figure>
									<div class="snipcart-item block">
										<div class="snipcart-thumb">
											<a href="<?php echo base_url('produtos/visualizar/'.$produto->id.'/'.$produto->produto);?>"><img src="<?php echo base_url('imagens/produtos/'.$produto->imagem);?>" alt="<?php echo $produto->produto;?>" class="img-responsive" /></a>
											<p><?php echo $produto->produto;?></p>
											<h4>R$ <?php echo number_format($produto->valor_promocao,2,",","."); ?> <span>R$ <?php echo number_format($produto->valor,2,",","."); ?></span></h4>
										</div>
										<div class="snipcart-details">
											<form action="#" method="post" class="form_produtos" id="form_produto_<?php echo $produto->id; ?>">
												<fieldset>
													<input type="hidden" name="cmd" value="_cart" />
													<input type="hidden" name="add" value="<?php echo $produto->id; ?>" />
													<input type="hidden" name="business" value="<?php echo $produto->id; ?>" />
													<input type="hidden" name="item_name" value="<?php echo $produto->produto; ?>" />
													<input type="hidden" name="amount" value="<?php echo $produto->valor; ?>" />
													<input type="hidden" name="discount_amount" value="<?php echo ($produto->valor - $produto->valor_promocao); ?>" />
													<input type="hidden" name="currency_code" value="BRL" />
													<input type="hidden" name="return" value=" " />
													<input type="hidden" name="cancel_return" value=" " />
													<input type="submit" name="submit" value="Adicionar ao Carrinho" class="button" />
												</fieldset>
											</form>
										</div>
									</div>
								</figure>
							</div>
						</div>
						</div>
					</div>
					<?php } ?>
					
					<div class="clearfix"> </div>
				</div>
							
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
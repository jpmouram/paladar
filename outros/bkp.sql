-- --------------------------------------------------------
-- Servidor:                     localhost
-- Versão do servidor:           10.1.26-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win32
-- HeidiSQL Versão:              9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Copiando estrutura do banco de dados para paladar
CREATE DATABASE IF NOT EXISTS `paladar` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `paladar`;


-- Copiando estrutura para tabela paladar.categorias
CREATE TABLE IF NOT EXISTS `categorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_categoria` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `categoria` varchar(50) NOT NULL DEFAULT '0',
  `imagem` varchar(50) NOT NULL,
  `caminho_imagem` varchar(255) NOT NULL,
  `data_add` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `id_categoria` (`id_categoria`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela paladar.categorias: ~15 rows (aproximadamente)
/*!40000 ALTER TABLE `categorias` DISABLE KEYS */;
INSERT INTO `categorias` (`id`, `id_categoria`, `status`, `categoria`, `imagem`, `caminho_imagem`, `data_add`) VALUES
	(1, 0, 0, 'teste', '0', '', '2017-11-24 03:14:44'),
	(2, 0, 1, 'Bebidas', '0', '', '2017-11-24 03:15:03'),
	(3, 0, 1, 'Frios', '0', '', '2017-11-24 03:15:13'),
	(4, 0, 0, 'Frios', '0', '', '0000-00-00 00:00:00'),
	(5, 0, 1, 'Laticínios', '0', '', '2017-11-24 15:20:51'),
	(6, 0, 1, 'Teste', '0', '', '2017-11-24 15:21:00'),
	(7, 6, 0, 'Nome da subcategoria', '0', '', '2017-11-24 15:49:22'),
	(8, 2, 1, 'Cervejas', '20170105123030_565999435_D.jpg', 'C:/xampp/htdocs/alex_sanches/paladar/sys/imagens/subcategorias/20170105123030_565999435_D.jpg', '2017-11-24 15:55:38'),
	(9, 2, 1, 'Refrigerantes', '0', '', '2017-11-24 15:55:50'),
	(10, 2, 1, 'Aguas', '0', '', '2017-11-24 15:55:53'),
	(11, 2, 1, 'Sucos', 'suco-uva-integral-dieta-corpo-emagrecer-corte.jpg', 'C:/xampp/htdocs/alex_sanches/paladar/sys/imagens/subcategorias/suco-uva-integral-dieta-corpo-emagrecer-corte.jpg', '2017-11-24 18:46:04'),
	(14, 2, 0, 'Teste', 'javascript_logo4.png', 'C:/xampp/htdocs/alex_sanches/paladar/sys/imagens/subcategorias/javascript_logo4.png', '2017-11-24 23:50:56'),
	(15, 2, 0, 'jjjj', 'javascript_logo3.png', 'C:/xampp/htdocs/alex_sanches/paladar/sys/imagens/s', '2017-11-25 00:02:43'),
	(16, 2, 0, 'Nova sub', 'photo1.jpg', 'C:/xampp/htdocs/alex_sanches/paladar/sys/imagens/s', '2017-11-25 01:06:33'),
	(17, 5, 1, 'Leite em pó', '1602753_01.jpg', 'C:/xampp/htdocs/alex_sanches/paladar/sys/imagens/subcategorias/1602753_01.jpg', '2017-11-27 05:22:18');
/*!40000 ALTER TABLE `categorias` ENABLE KEYS */;


-- Copiando estrutura para tabela paladar.produtos
CREATE TABLE IF NOT EXISTS `produtos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_sub_categoria` int(5) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '1',
  `codigo` bigint(20) NOT NULL,
  `valor` float(10,2) NOT NULL,
  `valor_promocao` float(10,2) NOT NULL,
  `produto` varchar(255) NOT NULL,
  `embalagem` varchar(10) NOT NULL,
  `imagem` varchar(255) NOT NULL,
  `caminho_imagem` varchar(255) NOT NULL,
  `descricao` text NOT NULL,
  `data_add` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `id_sub_categoria` (`id_sub_categoria`),
  KEY `status` (`status`),
  KEY `codigo` (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela paladar.produtos: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `produtos` DISABLE KEYS */;
INSERT INTO `produtos` (`id`, `id_sub_categoria`, `status`, `codigo`, `valor`, `valor_promocao`, `produto`, `embalagem`, `imagem`, `caminho_imagem`, `descricao`, `data_add`) VALUES
	(1, 11, 1, 123, 6.70, 5.90, 'Suco de Laranja', '', 'suco-de-laranja-para-pressao-alta_22000_l.jpg', 'C:/xampp/htdocs/alex_sanches/paladar/sys/imagens/produtos/suco-de-laranja-para-pressao-alta_22000_l.jpg', 'Testando desrição', '0000-00-00 00:00:00'),
	(2, 11, 0, 123, 24.78, 0.00, 'Produto Teste', '', '', '', 'Testando desrição', '0000-00-00 00:00:00'),
	(3, 8, 0, 23, 2041.22, 0.00, 'teste', '', '', '', '', '2017-11-26 23:21:23'),
	(6, 8, 1, 1, 9.70, 8.70, 'Original 600ml', '', '20170105123030_565999435_D.jpg', 'C:/xampp/htdocs/alex_sanches/paladar/sys/imagens/produtos/20170105123030_565999435_D.jpg', '', '2017-11-26 23:52:43'),
	(7, 11, 1, 21, 3.34, 0.00, 'Suco de Uva', '', 'suco-uva-integral-dieta-corpo-emagrecer-corte1.jpg', 'C:/xampp/htdocs/alex_sanches/paladar/sys/imagens/produtos/suco-uva-integral-dieta-corpo-emagrecer-corte1.jpg', '', '2017-11-27 05:20:43'),
	(8, 17, 1, 22, 4.04, 0.00, 'Molico em pó 280g', '', '1602753_01.jpg', 'C:/xampp/htdocs/alex_sanches/paladar/sys/imagens/produtos/1602753_01.jpg', '', '2017-11-27 05:22:38');
/*!40000 ALTER TABLE `produtos` ENABLE KEYS */;


-- Copiando estrutura para tabela paladar.usuarios
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` enum('Y','N') NOT NULL DEFAULT 'Y',
  `email` varchar(100) NOT NULL,
  `senha` varchar(100) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `telefone` varchar(20) NOT NULL,
  `celular` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `status` (`status`),
  KEY `email` (`email`),
  KEY `senha` (`senha`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela paladar.usuarios: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` (`id`, `status`, `email`, `senha`, `nome`, `telefone`, `celular`) VALUES
	(1, 'Y', 'joaopaulomouram@gmail.com', '1234', 'João Paulo Moura', '(11) 97334-5329', '(11) 97334-5329');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
